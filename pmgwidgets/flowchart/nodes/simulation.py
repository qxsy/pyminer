import sys
from typing import List

from qtpy.QtWidgets import QApplication, QDialog, QVBoxLayout, QLineEdit
from pmgwidgets import PMFlowWidget, PMGFlowContent


class BaseLigralGenerator(PMGFlowContent):
    def __init__(self):
        super(PMGFlowContent, self).__init__()
        self.input_args_labels = ['input1']
        self.output_ports_labels = ['output1', 'output2']
        self.class_name = 'LigralGenerator'
        self.text = 'Ligral生成器'
        self.icon_path = ''
