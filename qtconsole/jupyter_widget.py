"""A FrontendWidget that emulates a repl for a Jupyter kernel.

This supports the additional functionality provided by Jupyter kernel.
"""

# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.

from collections import namedtuple
import os.path
import re
from subprocess import Popen
import sys
import time

from textwrap import dedent
from warnings import warn

from qtpy import QtCore, QtGui
t0 = time.time()
#############
# from IPython.lib.lexers import IPythonLexer, IPython3Lexer
# -*- coding: utf-8 -*-
"""
Defines a variety of Pygments lexers for highlighting IPython code.

This includes:

    IPythonLexer, IPython3Lexer
        Lexers for pure IPython (python + magic/shell commands)

    IPythonPartialTracebackLexer, IPythonTracebackLexer
        Supports 2.x and 3.x via keyword `python3`.  The partial traceback
        lexer reads everything but the Python code appearing in a traceback.
        The full lexer combines the partial lexer with an IPython lexer.

    IPythonConsoleLexer
        A lexer for IPython console sessions, with support for tracebacks.

    IPyLexer
        A friendly lexer which examines the first line of text and from it,
        decides whether to use an IPython lexer or an IPython console lexer.
        This is probably the only lexer that needs to be explicitly added
        to Pygments.

"""
# -----------------------------------------------------------------------------
# Copyright (c) 2013, the IPython Development Team.
#
# Distributed under the terms of the Modified BSD License.
#
# The full license is in the file COPYING.txt, distributed with this software.
# -----------------------------------------------------------------------------

# Standard library
import re

# Third party
from pygments.lexers import (
    BashLexer, HtmlLexer, JavascriptLexer, RubyLexer, PerlLexer, PythonLexer,
    Python3Lexer, TexLexer)
from pygments.lexer import (
    Lexer, DelegatingLexer, RegexLexer, do_insertions, bygroups, using,
)
from pygments.token import (
    Generic, Keyword, Literal, Name, Operator, Other, Text, Error,
)
from pygments.util import get_bool_opt

# Local

line_re = re.compile('.*?\n')

__all__ = ['build_ipy_lexer', 'IPython3Lexer', 'IPythonLexer',
           'IPythonPartialTracebackLexer', 'IPythonTracebackLexer',
           'IPythonConsoleLexer', 'IPyLexer']


def build_ipy_lexer(python3):
    """Builds IPython lexers depending on the value of `python3`.

    The lexer inherits from an appropriate Python lexer and then adds
    information about IPython specific keywords (i.e. magic commands,
    shell commands, etc.)

    Parameters
    ----------
    python3 : bool
        If `True`, then build an IPython lexer from a Python 3 lexer.

    """
    # It would be nice to have a single IPython lexer class which takes
    # a boolean `python3`.  But since there are two Python lexer classes,
    # we will also have two IPython lexer classes.
    if python3:
        PyLexer = Python3Lexer
        name = 'IPython3'
        aliases = ['ipython3']
        doc = """IPython3 Lexer"""
    else:
        PyLexer = PythonLexer
        name = 'IPython'
        aliases = ['ipython2', 'ipython']
        doc = """IPython Lexer"""

    ipython_tokens = [
        (r'(?s)(\s*)(%%capture)([^\n]*\n)(.*)', bygroups(Text, Operator, Text, using(PyLexer))),
        (r'(?s)(\s*)(%%debug)([^\n]*\n)(.*)', bygroups(Text, Operator, Text, using(PyLexer))),
        (r'(?is)(\s*)(%%html)([^\n]*\n)(.*)', bygroups(Text, Operator, Text, using(HtmlLexer))),
        (r'(?s)(\s*)(%%javascript)([^\n]*\n)(.*)', bygroups(Text, Operator, Text, using(JavascriptLexer))),
        (r'(?s)(\s*)(%%js)([^\n]*\n)(.*)', bygroups(Text, Operator, Text, using(JavascriptLexer))),
        (r'(?s)(\s*)(%%latex)([^\n]*\n)(.*)', bygroups(Text, Operator, Text, using(TexLexer))),
        (r'(?s)(\s*)(%%perl)([^\n]*\n)(.*)', bygroups(Text, Operator, Text, using(PerlLexer))),
        (r'(?s)(\s*)(%%prun)([^\n]*\n)(.*)', bygroups(Text, Operator, Text, using(PyLexer))),
        (r'(?s)(\s*)(%%pypy)([^\n]*\n)(.*)', bygroups(Text, Operator, Text, using(PyLexer))),
        (r'(?s)(\s*)(%%python)([^\n]*\n)(.*)', bygroups(Text, Operator, Text, using(PyLexer))),
        (r'(?s)(\s*)(%%python2)([^\n]*\n)(.*)', bygroups(Text, Operator, Text, using(PythonLexer))),
        (r'(?s)(\s*)(%%python3)([^\n]*\n)(.*)', bygroups(Text, Operator, Text, using(Python3Lexer))),
        (r'(?s)(\s*)(%%ruby)([^\n]*\n)(.*)', bygroups(Text, Operator, Text, using(RubyLexer))),
        (r'(?s)(\s*)(%%time)([^\n]*\n)(.*)', bygroups(Text, Operator, Text, using(PyLexer))),
        (r'(?s)(\s*)(%%timeit)([^\n]*\n)(.*)', bygroups(Text, Operator, Text, using(PyLexer))),
        (r'(?s)(\s*)(%%writefile)([^\n]*\n)(.*)', bygroups(Text, Operator, Text, using(PyLexer))),
        (r'(?s)(\s*)(%%file)([^\n]*\n)(.*)', bygroups(Text, Operator, Text, using(PyLexer))),
        (r"(?s)(\s*)(%%)(\w+)(.*)", bygroups(Text, Operator, Keyword, Text)),
        (r'(?s)(^\s*)(%%!)([^\n]*\n)(.*)', bygroups(Text, Operator, Text, using(BashLexer))),
        (r"(%%?)(\w+)(\?\??)$", bygroups(Operator, Keyword, Operator)),
        (r"\b(\?\??)(\s*)$", bygroups(Operator, Text)),
        (r'(%)(sx|sc|system)(.*)(\n)', bygroups(Operator, Keyword,
                                                using(BashLexer), Text)),
        (r'(%)(\w+)(.*\n)', bygroups(Operator, Keyword, Text)),
        (r'^(!!)(.+)(\n)', bygroups(Operator, using(BashLexer), Text)),
        (r'(!)(?!=)(.+)(\n)', bygroups(Operator, using(BashLexer), Text)),
        (r'^(\s*)(\?\??)(\s*%{0,2}[\w\.\*]*)', bygroups(Text, Operator, Text)),
        (r'(\s*%{0,2}[\w\.\*]*)(\?\??)(\s*)$', bygroups(Text, Operator, Text)),
    ]

    tokens = PyLexer.tokens.copy()
    tokens['root'] = ipython_tokens + tokens['root']

    attrs = {'name': name, 'aliases': aliases, 'filenames': [],
             '__doc__': doc, 'tokens': tokens}

    return type(name, (PyLexer,), attrs)


IPython3Lexer = build_ipy_lexer(python3=True)
IPythonLexer = build_ipy_lexer(python3=False)


class IPythonPartialTracebackLexer(RegexLexer):
    """
    Partial lexer for IPython tracebacks.

    Handles all the non-python output.

    """
    name = 'IPython Partial Traceback'

    tokens = {
        'root': [
            # Tracebacks for syntax errors have a different style.
            # For both types of tracebacks, we mark the first line with
            # Generic.Traceback.  For syntax errors, we mark the filename
            # as we mark the filenames for non-syntax tracebacks.
            #
            # These two regexps define how IPythonConsoleLexer finds a
            # traceback.
            #
            ## Non-syntax traceback
            (r'^(\^C)?(-+\n)', bygroups(Error, Generic.Traceback)),
            ## Syntax traceback
            (r'^(  File)(.*)(, line )(\d+\n)',
             bygroups(Generic.Traceback, Name.Namespace,
                      Generic.Traceback, Literal.Number.Integer)),

            # (Exception Identifier)(Whitespace)(Traceback Message)
            (r'(?u)(^[^\d\W]\w*)(\s*)(Traceback.*?\n)',
             bygroups(Name.Exception, Generic.Whitespace, Text)),
            # (Module/Filename)(Text)(Callee)(Function Signature)
            # Better options for callee and function signature?
            (r'(.*)( in )(.*)(\(.*\)\n)',
             bygroups(Name.Namespace, Text, Name.Entity, Name.Tag)),
            # Regular line: (Whitespace)(Line Number)(Python Code)
            (r'(\s*?)(\d+)(.*?\n)',
             bygroups(Generic.Whitespace, Literal.Number.Integer, Other)),
            # Emphasized line: (Arrow)(Line Number)(Python Code)
            # Using Exception token so arrow color matches the Exception.
            (r'(-*>?\s?)(\d+)(.*?\n)',
             bygroups(Name.Exception, Literal.Number.Integer, Other)),
            # (Exception Identifier)(Message)
            (r'(?u)(^[^\d\W]\w*)(:.*?\n)',
             bygroups(Name.Exception, Text)),
            # Tag everything else as Other, will be handled later.
            (r'.*\n', Other),
        ],
    }


class IPythonTracebackLexer(DelegatingLexer):
    """
    IPython traceback lexer.

    For doctests, the tracebacks can be snipped as much as desired with the
    exception to the lines that designate a traceback. For non-syntax error
    tracebacks, this is the line of hyphens. For syntax error tracebacks,
    this is the line which lists the File and line number.

    """
    # The lexer inherits from DelegatingLexer.  The "root" lexer is an
    # appropriate IPython lexer, which depends on the value of the boolean
    # `python3`.  First, we parse with the partial IPython traceback lexer.
    # Then, any code marked with the "Other" token is delegated to the root
    # lexer.
    #
    name = 'IPython Traceback'
    aliases = ['ipythontb']

    def __init__(self, **options):
        self.python3 = get_bool_opt(options, 'python3', False)
        if self.python3:
            self.aliases = ['ipython3tb']
        else:
            self.aliases = ['ipython2tb', 'ipythontb']

        if self.python3:
            IPyLexer = IPython3Lexer
        else:
            IPyLexer = IPythonLexer

        DelegatingLexer.__init__(self, IPyLexer,
                                 IPythonPartialTracebackLexer, **options)


class IPythonConsoleLexer(Lexer):
    """
    An IPython console lexer for IPython code-blocks and doctests, such as:

    .. code-block:: rst

        .. code-block:: ipythonconsole

            In [1]: a = 'foo'

            In [2]: a
            Out[2]: 'foo'

            In [3]: print a
            foo

            In [4]: 1 / 0


    Support is also provided for IPython exceptions:

    .. code-block:: rst

        .. code-block:: ipythonconsole

            In [1]: raise Exception

            ---------------------------------------------------------------------------
            Exception                                 Traceback (most recent call last)
            <ipython-input-1-fca2ab0ca76b> in <module>
            ----> 1 raise Exception

            Exception:

    """
    name = 'IPython console session'
    aliases = ['ipythonconsole']
    mimetypes = ['text/x-ipython-console']

    # The regexps used to determine what is input and what is output.
    # The default prompts for IPython are:
    #
    #    in           = 'In [#]: '
    #    continuation = '   .D.: '
    #    template     = 'Out[#]: '
    #
    # Where '#' is the 'prompt number' or 'execution count' and 'D'
    # D is a number of dots  matching the width of the execution count
    #
    in1_regex = r'In \[[0-9]+\]: '
    in2_regex = r'   \.\.+\.: '
    out_regex = r'Out\[[0-9]+\]: '

    #: The regex to determine when a traceback starts.
    ipytb_start = re.compile(r'^(\^C)?(-+\n)|^(  File)(.*)(, line )(\d+\n)')

    def __init__(self, **options):
        """Initialize the IPython console lexer.

        Parameters
        ----------
        python3 : bool
            If `True`, then the console inputs are parsed using a Python 3
            lexer. Otherwise, they are parsed using a Python 2 lexer.
        in1_regex : RegexObject
            The compiled regular expression used to detect the start
            of inputs. Although the IPython configuration setting may have a
            trailing whitespace, do not include it in the regex. If `None`,
            then the default input prompt is assumed.
        in2_regex : RegexObject
            The compiled regular expression used to detect the continuation
            of inputs. Although the IPython configuration setting may have a
            trailing whitespace, do not include it in the regex. If `None`,
            then the default input prompt is assumed.
        out_regex : RegexObject
            The compiled regular expression used to detect outputs. If `None`,
            then the default output prompt is assumed.

        """
        self.python3 = get_bool_opt(options, 'python3', False)
        if self.python3:
            self.aliases = ['ipython3console']
        else:
            self.aliases = ['ipython2console', 'ipythonconsole']

        in1_regex = options.get('in1_regex', self.in1_regex)
        in2_regex = options.get('in2_regex', self.in2_regex)
        out_regex = options.get('out_regex', self.out_regex)

        # So that we can work with input and output prompts which have been
        # rstrip'd (possibly by editors) we also need rstrip'd variants. If
        # we do not do this, then such prompts will be tagged as 'output'.
        # The reason can't just use the rstrip'd variants instead is because
        # we want any whitespace associated with the prompt to be inserted
        # with the token. This allows formatted code to be modified so as hide
        # the appearance of prompts, with the whitespace included. One example
        # use of this is in copybutton.js from the standard lib Python docs.
        in1_regex_rstrip = in1_regex.rstrip() + '\n'
        in2_regex_rstrip = in2_regex.rstrip() + '\n'
        out_regex_rstrip = out_regex.rstrip() + '\n'

        # Compile and save them all.
        attrs = ['in1_regex', 'in2_regex', 'out_regex',
                 'in1_regex_rstrip', 'in2_regex_rstrip', 'out_regex_rstrip']
        for attr in attrs:
            self.__setattr__(attr, re.compile(locals()[attr]))

        Lexer.__init__(self, **options)

        if self.python3:
            pylexer = IPython3Lexer
            tblexer = IPythonTracebackLexer
        else:
            pylexer = IPythonLexer
            tblexer = IPythonTracebackLexer

        self.pylexer = pylexer(**options)
        self.tblexer = tblexer(**options)

        self.reset()

    def reset(self):
        self.mode = 'output'
        self.index = 0
        self.buffer = u''
        self.insertions = []

    def buffered_tokens(self):
        """
        Generator of unprocessed tokens after doing insertions and before
        changing to a new state.

        """
        if self.mode == 'output':
            tokens = [(0, Generic.Output, self.buffer)]
        elif self.mode == 'input':
            tokens = self.pylexer.get_tokens_unprocessed(self.buffer)
        else:  # traceback
            tokens = self.tblexer.get_tokens_unprocessed(self.buffer)

        for i, t, v in do_insertions(self.insertions, tokens):
            # All token indexes are relative to the buffer.
            yield self.index + i, t, v

        # Clear it all
        self.index += len(self.buffer)
        self.buffer = u''
        self.insertions = []

    def get_mci(self, line):
        """
        Parses the line and returns a 3-tuple: (mode, code, insertion).

        `mode` is the next mode (or state) of the lexer, and is always equal
        to 'input', 'output', or 'tb'.

        `code` is a portion of the line that should be added to the buffer
        corresponding to the next mode and eventually lexed by another lexer.
        For example, `code` could be Python code if `mode` were 'input'.

        `insertion` is a 3-tuple (index, token, text) representing an
        unprocessed "token" that will be inserted into the stream of tokens
        that are created from the buffer once we change modes. This is usually
        the input or output prompt.

        In general, the next mode depends on current mode and on the contents
        of `line`.

        """
        # To reduce the number of regex match checks, we have multiple
        # 'if' blocks instead of 'if-elif' blocks.

        # Check for possible end of input
        in2_match = self.in2_regex.match(line)
        in2_match_rstrip = self.in2_regex_rstrip.match(line)
        if (in2_match and in2_match.group().rstrip() == line.rstrip()) or \
                in2_match_rstrip:
            end_input = True
        else:
            end_input = False
        if end_input and self.mode != 'tb':
            # Only look for an end of input when not in tb mode.
            # An ellipsis could appear within the traceback.
            mode = 'output'
            code = u''
            insertion = (0, Generic.Prompt, line)
            return mode, code, insertion

        # Check for output prompt
        out_match = self.out_regex.match(line)
        out_match_rstrip = self.out_regex_rstrip.match(line)
        if out_match or out_match_rstrip:
            mode = 'output'
            if out_match:
                idx = out_match.end()
            else:
                idx = out_match_rstrip.end()
            code = line[idx:]
            # Use the 'heading' token for output.  We cannot use Generic.Error
            # since it would conflict with exceptions.
            insertion = (0, Generic.Heading, line[:idx])
            return mode, code, insertion

        # Check for input or continuation prompt (non stripped version)
        in1_match = self.in1_regex.match(line)
        if in1_match or (in2_match and self.mode != 'tb'):
            # New input or when not in tb, continued input.
            # We do not check for continued input when in tb since it is
            # allowable to replace a long stack with an ellipsis.
            mode = 'input'
            if in1_match:
                idx = in1_match.end()
            else:  # in2_match
                idx = in2_match.end()
            code = line[idx:]
            insertion = (0, Generic.Prompt, line[:idx])
            return mode, code, insertion

        # Check for input or continuation prompt (stripped version)
        in1_match_rstrip = self.in1_regex_rstrip.match(line)
        if in1_match_rstrip or (in2_match_rstrip and self.mode != 'tb'):
            # New input or when not in tb, continued input.
            # We do not check for continued input when in tb since it is
            # allowable to replace a long stack with an ellipsis.
            mode = 'input'
            if in1_match_rstrip:
                idx = in1_match_rstrip.end()
            else:  # in2_match
                idx = in2_match_rstrip.end()
            code = line[idx:]
            insertion = (0, Generic.Prompt, line[:idx])
            return mode, code, insertion

        # Check for traceback
        if self.ipytb_start.match(line):
            mode = 'tb'
            code = line
            insertion = None
            return mode, code, insertion

        # All other stuff...
        if self.mode in ('input', 'output'):
            # We assume all other text is output. Multiline input that
            # does not use the continuation marker cannot be detected.
            # For example, the 3 in the following is clearly output:
            #
            #    In [1]: print 3
            #    3
            #
            # But the following second line is part of the input:
            #
            #    In [2]: while True:
            #        print True
            #
            # In both cases, the 2nd line will be 'output'.
            #
            mode = 'output'
        else:
            mode = 'tb'

        code = line
        insertion = None

        return mode, code, insertion

    def get_tokens_unprocessed(self, text):
        self.reset()
        for match in line_re.finditer(text):
            line = match.group()
            mode, code, insertion = self.get_mci(line)

            if mode != self.mode:
                # Yield buffered tokens before transitioning to new mode.
                for token in self.buffered_tokens():
                    yield token
                self.mode = mode

            if insertion:
                self.insertions.append((len(self.buffer), [insertion]))
            self.buffer += code

        for token in self.buffered_tokens():
            yield token


class IPyLexer(Lexer):
    r"""
    Primary lexer for all IPython-like code.

    This is a simple helper lexer.  If the first line of the text begins with
    "In \[[0-9]+\]:", then the entire text is parsed with an IPython console
    lexer. If not, then the entire text is parsed with an IPython lexer.

    The goal is to reduce the number of lexers that are registered
    with Pygments.

    """
    name = 'IPy session'
    aliases = ['ipy']

    def __init__(self, **options):
        self.python3 = get_bool_opt(options, 'python3', False)
        if self.python3:
            self.aliases = ['ipy3']
        else:
            self.aliases = ['ipy2', 'ipy']

        Lexer.__init__(self, **options)

        self.IPythonLexer = IPythonLexer(**options)
        self.IPythonConsoleLexer = IPythonConsoleLexer(**options)

    def get_tokens_unprocessed(self, text):
        # Search for the input prompt anywhere...this allows code blocks to
        # begin with comments as well.
        if re.match(r'.*(In \[[0-9]+\]:)', text.strip(), re.DOTALL):
            lex = self.IPythonConsoleLexer
        else:
            lex = self.IPythonLexer
        for token in lex.get_tokens_unprocessed(text):
            yield token
####################
from pygments.lexers import get_lexer_by_name
from pygments.util import ClassNotFound

from qtconsole import __version__
from traitlets import Bool, Unicode, observe, default



from .frontend_widget import FrontendWidget


from . import styles
# -----------------------------------------------------------------------------
# Constants
# -----------------------------------------------------------------------------

# Default strings to build and display input and output prompts (and separators
# in between)
default_in_prompt = 'In [<span class="in-prompt-number">%i</span>]: '
default_out_prompt = 'Out[<span class="out-prompt-number">%i</span>]: '
default_input_sep = '\n'
default_output_sep = ''
default_output_sep2 = ''

# Base path for most payload sources.
zmq_shell_source = 'ipykernel.zmqshell.ZMQInteractiveShell'

if sys.platform.startswith('win'):
    default_editor = 'notepad'
else:
    default_editor = ''


# -----------------------------------------------------------------------------
# JupyterWidget class
# -----------------------------------------------------------------------------

class IPythonWidget(FrontendWidget):
    """Dummy class for config inheritance. Destroyed below."""


class JupyterWidget(IPythonWidget):
    """A FrontendWidget for a Jupyter kernel."""

    # If set, the 'custom_edit_requested(str, int)' signal will be emitted when
    # an editor is needed for a file. This overrides 'editor' and 'editor_line'
    # settings.
    custom_edit = Bool(False)
    custom_edit_requested = QtCore.Signal(object, object)

    editor = Unicode(default_editor, config=True,
                     help="""
        A command for invoking a GUI text editor. If the string contains a
        {filename} format specifier, it will be used. Otherwise, the filename
        will be appended to the end the command. To use a terminal text editor,
        the command should launch a new terminal, e.g.
        ``"gnome-terminal -- vim"``.
        """)

    editor_line = Unicode(config=True,
                          help="""
        The editor command to use when a specific line number is requested. The
        string should contain two format specifiers: {line} and {filename}. If
        this parameter is not specified, the line number option to the %edit
        magic will be ignored.
        """)

    style_sheet = Unicode(config=True,
                          help="""
        A CSS stylesheet. The stylesheet can contain classes for:
            1. Qt: QPlainTextEdit, QFrame, QWidget, etc
            2. Pygments: .c, .k, .o, etc. (see PygmentsHighlighter)
            3. QtConsole: .error, .in-prompt, .out-prompt, etc
        """)

    syntax_style = Unicode(config=True,
                           help="""
        If not empty, use this Pygments style for syntax highlighting.
        Otherwise, the style sheet is queried for Pygments style
        information.
        """)

    # Prompts.
    in_prompt = Unicode(default_in_prompt, config=True)
    out_prompt = Unicode(default_out_prompt, config=True)
    input_sep = Unicode(default_input_sep, config=True)
    output_sep = Unicode(default_output_sep, config=True)
    output_sep2 = Unicode(default_output_sep2, config=True)

    # JupyterWidget protected class variables.
    _PromptBlock = namedtuple('_PromptBlock', ['block', 'length', 'number'])
    _payload_source_edit = 'edit_magic'
    _payload_source_exit = 'ask_exit'
    _payload_source_next_input = 'set_next_input'
    _payload_source_page = 'page'
    _retrying_history_request = False
    _starting = False

    # ---------------------------------------------------------------------------
    # 'object' interface
    # ---------------------------------------------------------------------------

    def __init__(self, *args, **kw):
        super(JupyterWidget, self).__init__(*args, **kw)

        # JupyterWidget protected variables.
        self._payload_handlers = {
            self._payload_source_edit: self._handle_payload_edit,
            self._payload_source_exit: self._handle_payload_exit,
            self._payload_source_page: self._handle_payload_page,
            self._payload_source_next_input: self._handle_payload_next_input}
        self._previous_prompt_obj = None
        self._keep_kernel_on_exit = None

        # Initialize widget styling.
        if self.style_sheet:
            self._style_sheet_changed()
            self._syntax_style_changed()
        else:
            self.set_default_style()

        # Initialize language name.
        self.language_name = None

    # ---------------------------------------------------------------------------
    # 'BaseFrontendMixin' abstract interface
    #
    # For JupyterWidget,  override FrontendWidget methods which implement the
    # BaseFrontend Mixin abstract interface
    # ---------------------------------------------------------------------------

    def _handle_complete_reply(self, rep):
        """Support Jupyter's improved completion machinery.
        """
        self.log.debug("complete: %s", rep.get('content', ''))
        cursor = self._get_cursor()
        info = self._request_info.get('complete')
        if (info and info.id == rep['parent_header']['msg_id']
                and info.pos == self._get_input_buffer_cursor_pos()
                and info.code == self.input_buffer):
            content = rep['content']
            matches = content['matches']
            start = content['cursor_start']
            end = content['cursor_end']

            start = max(start, 0)
            end = max(end, start)

            # Move the control's cursor to the desired end point
            cursor_pos = self._get_input_buffer_cursor_pos()
            if end < cursor_pos:
                cursor.movePosition(QtGui.QTextCursor.Left,
                                    n=(cursor_pos - end))
            elif end > cursor_pos:
                cursor.movePosition(QtGui.QTextCursor.Right,
                                    n=(end - cursor_pos))
            # This line actually applies the move to control's cursor
            self._control.setTextCursor(cursor)

            offset = end - start
            # Move the local cursor object to the start of the match and
            # complete.
            cursor.movePosition(QtGui.QTextCursor.Left, n=offset)
            self._complete_with_items(cursor, matches)

    def _handle_execute_reply(self, msg):
        """Support prompt requests.
        """
        msg_id = msg['parent_header'].get('msg_id')
        info = self._request_info['execute'].get(msg_id)
        if info and info.kind == 'prompt':
            content = msg['content']
            if content['status'] == 'aborted':
                self._show_interpreter_prompt()
            else:
                number = content['execution_count'] + 1
                self._show_interpreter_prompt(number)
            self._request_info['execute'].pop(msg_id)
        else:
            super(JupyterWidget, self)._handle_execute_reply(msg)

    def _handle_history_reply(self, msg):
        """ Handle history tail replies, which are only supported
            by Jupyter kernels.
        """
        content = msg['content']
        if 'history' not in content:
            self.log.error("History request failed: %r" % content)
            if content.get('status', '') == 'aborted' and \
                    not self._retrying_history_request:
                # a *different* action caused this request to be aborted, so
                # we should try again.
                self.log.error("Retrying aborted history request")
                # prevent multiple retries of aborted requests:
                self._retrying_history_request = True
                # wait out the kernel's queue flush, which is currently timed at 0.1s
                time.sleep(0.25)
                self.kernel_client.history(hist_access_type='tail', n=1000)
            else:
                self._retrying_history_request = False
            return
        # reset retry flag
        self._retrying_history_request = False
        history_items = content['history']
        self.log.debug("Received history reply with %i entries", len(history_items))
        items = []
        last_cell = u""
        for _, _, cell in history_items:
            cell = cell.rstrip()
            if cell != last_cell:
                items.append(cell)
                last_cell = cell
        self._set_history(items)

    def _insert_other_input(self, cursor, content, remote=True):
        """Insert function for input from other frontends"""
        n = content.get('execution_count', 0)
        prompt = self._make_in_prompt(n, remote=remote)
        cont_prompt = self._make_continuation_prompt(self._prompt, remote=remote)
        cursor.insertText('\n')
        for i, line in enumerate(content['code'].strip().split('\n')):
            if i == 0:
                self._insert_html(cursor, prompt)
            else:
                self._insert_html(cursor, cont_prompt)
            self._insert_plain_text(cursor, line + '\n')

        # Update current prompt number
        self._update_prompt(n + 1)

    def _handle_execute_input(self, msg):
        """Handle an execute_input message"""
        self.log.debug("execute_input: %s", msg.get('content', ''))
        if self.include_output(msg):
            self._append_custom(
                self._insert_other_input, msg['content'], before_prompt=True)
        elif not self._prompt:
            self._append_custom(
                self._insert_other_input, msg['content'],
                before_prompt=True, remote=False)

    def _handle_execute_result(self, msg):
        """Handle an execute_result message"""
        self.log.debug("execute_result: %s", msg.get('content', ''))
        if self.include_output(msg):
            self.flush_clearoutput()
            content = msg['content']
            prompt_number = content.get('execution_count', 0)
            data = content['data']
            if 'text/plain' in data:
                self._append_plain_text(self.output_sep, before_prompt=True)
                self._append_html(
                    self._make_out_prompt(prompt_number, remote=not self.from_here(msg)),
                    before_prompt=True
                )
                text = data['text/plain']
                # If the repr is multiline, make sure we start on a new line,
                # so that its lines are aligned.
                if "\n" in text and not self.output_sep.endswith("\n"):
                    self._append_plain_text('\n', before_prompt=True)
                self._append_plain_text(text + self.output_sep2, before_prompt=True)

                if not self.from_here(msg):
                    self._append_plain_text('\n', before_prompt=True)

    def _handle_display_data(self, msg):
        """The base handler for the ``display_data`` message."""
        # For now, we don't display data from other frontends, but we
        # eventually will as this allows all frontends to monitor the display
        # data. But we need to figure out how to handle this in the GUI.
        if self.include_output(msg):
            self.flush_clearoutput()
            data = msg['content']['data']
            metadata = msg['content']['metadata']
            # In the regular JupyterWidget, we simply print the plain text
            # representation.
            if 'text/plain' in data:
                text = data['text/plain']
                self._append_plain_text(text, True)
            # This newline seems to be needed for text and html output.
            self._append_plain_text(u'\n', True)

    def _handle_kernel_info_reply(self, rep):
        """Handle kernel info replies."""
        content = rep['content']
        self.language_name = content['language_info']['name']
        pygments_lexer = content['language_info'].get('pygments_lexer', '')

        try:
            # Other kernels with pygments_lexer info will have to be
            # added here by hand.
            if pygments_lexer == 'ipython3':
                lexer = IPython3Lexer()
            elif pygments_lexer == 'ipython2':
                lexer = IPythonLexer()
            else:
                lexer = get_lexer_by_name(self.language_name)
            self._highlighter._lexer = lexer
        except ClassNotFound:
            pass

        self.kernel_banner = content.get('banner', '')
        if self._starting:
            # finish handling started channels
            self._starting = False
            super(JupyterWidget, self)._started_channels()

    def _started_channels(self):
        """Make a history request"""
        self._starting = True
        self.kernel_client.kernel_info()
        self.kernel_client.history(hist_access_type='tail', n=1000)

    # ---------------------------------------------------------------------------
    # 'FrontendWidget' protected interface
    # ---------------------------------------------------------------------------

    def _process_execute_error(self, msg):
        """Handle an execute_error message"""
        self.log.debug("execute_error: %s", msg.get('content', ''))

        content = msg['content']

        traceback = '\n'.join(content['traceback']) + '\n'
        if False:
            # FIXME: For now, tracebacks come as plain text, so we can't
            # use the html renderer yet.  Once we refactor ultratb to
            # produce properly styled tracebacks, this branch should be the
            # default
            traceback = traceback.replace(' ', '&nbsp;')
            traceback = traceback.replace('\n', '<br/>')

            ename = content['ename']
            ename_styled = '<span class="error">%s</span>' % ename
            traceback = traceback.replace(ename, ename_styled)

            self._append_html(traceback)
        else:
            # This is the fallback for now, using plain text with ansi
            # escapes
            self._append_plain_text(traceback, before_prompt=not self.from_here(msg))

    def _process_execute_payload(self, item):
        """ Reimplemented to dispatch payloads to handler methods.
        """
        handler = self._payload_handlers.get(item['source'])
        if handler is None:
            # We have no handler for this type of payload, simply ignore it
            return False
        else:
            handler(item)
            return True

    def _show_interpreter_prompt(self, number=None):
        """ Reimplemented for IPython-style prompts.
        """
        # If a number was not specified, make a prompt number request.
        if number is None:
            msg_id = self.kernel_client.execute('', silent=True)
            info = self._ExecutionRequest(msg_id, 'prompt')
            self._request_info['execute'][msg_id] = info
            return

        # Show a new prompt and save information about it so that it can be
        # updated later if the prompt number turns out to be wrong.
        self._prompt_sep = self.input_sep
        self._show_prompt(self._make_in_prompt(number), html=True)
        block = self._control.document().lastBlock()
        length = len(self._prompt)
        self._previous_prompt_obj = self._PromptBlock(block, length, number)

        # Update continuation prompt to reflect (possibly) new prompt length.
        self._set_continuation_prompt(
            self._make_continuation_prompt(self._prompt), html=True)

    def _update_prompt(self, new_prompt_number):
        """Replace the last displayed prompt with a new one."""
        if self._previous_prompt_obj is None:
            return

        block = self._previous_prompt_obj.block

        # Make sure the prompt block has not been erased.
        if block.isValid() and block.text():
            # Remove the old prompt and insert a new prompt.
            cursor = QtGui.QTextCursor(block)
            cursor.movePosition(QtGui.QTextCursor.Right,
                                QtGui.QTextCursor.KeepAnchor,
                                self._previous_prompt_obj.length)
            prompt = self._make_in_prompt(new_prompt_number)
            self._prompt = self._insert_html_fetching_plain_text(
                cursor, prompt)

            # When the HTML is inserted, Qt blows away the syntax
            # highlighting for the line, so we need to rehighlight it.
            self._highlighter.rehighlightBlock(cursor.block())

            # Update the prompt cursor
            self._prompt_cursor.setPosition(cursor.position() - 1)

            # Store the updated prompt.
            block = self._control.document().lastBlock()
            length = len(self._prompt)
            self._previous_prompt_obj = self._PromptBlock(block, length, new_prompt_number)

    def _show_interpreter_prompt_for_reply(self, msg):
        """ Reimplemented for IPython-style prompts.
        """
        # Update the old prompt number if necessary.
        content = msg['content']
        # abort replies do not have any keys:
        if content['status'] == 'aborted':
            if self._previous_prompt_obj:
                previous_prompt_number = self._previous_prompt_obj.number
            else:
                previous_prompt_number = 0
        else:
            previous_prompt_number = content['execution_count']
        if self._previous_prompt_obj and \
                self._previous_prompt_obj.number != previous_prompt_number:
            self._update_prompt(previous_prompt_number)
            self._previous_prompt_obj = None

        # Show a new prompt with the kernel's estimated prompt number.
        self._show_interpreter_prompt(previous_prompt_number + 1)

    # ---------------------------------------------------------------------------
    # 'JupyterWidget' interface
    # ---------------------------------------------------------------------------

    def set_default_style(self, colors='lightbg'):
        """ Sets the widget style to the class defaults.

        Parameters
        ----------
        colors : str, optional (default lightbg)
            Whether to use the default light background or dark
            background or B&W style.
        """
        colors = colors.lower()
        if colors == 'lightbg':
            self.style_sheet = styles.default_light_style_sheet
            self.syntax_style = styles.default_light_syntax_style
        elif colors == 'linux':
            self.style_sheet = styles.default_dark_style_sheet
            self.syntax_style = styles.default_dark_syntax_style
        elif colors == 'nocolor':
            self.style_sheet = styles.default_bw_style_sheet
            self.syntax_style = styles.default_bw_syntax_style
        else:
            raise KeyError("No such color scheme: %s" % colors)

    # ---------------------------------------------------------------------------
    # 'JupyterWidget' protected interface
    # ---------------------------------------------------------------------------

    def _edit(self, filename, line=None):
        """ Opens a Python script for editing.

        Parameters
        ----------
        filename : str
            A path to a local system file.

        line : int, optional
            A line of interest in the file.
        """
        if self.custom_edit:
            self.custom_edit_requested.emit(filename, line)
        elif not self.editor:
            self._append_plain_text('No default editor available.\n'
                                    'Specify a GUI text editor in the `JupyterWidget.editor` '
                                    'configurable to enable the %edit magic')
        else:
            try:
                filename = '"%s"' % filename
                if line and self.editor_line:
                    command = self.editor_line.format(filename=filename,
                                                      line=line)
                else:
                    try:
                        command = self.editor.format()
                    except KeyError:
                        command = self.editor.format(filename=filename)
                    else:
                        command += ' ' + filename
            except KeyError:
                self._append_plain_text('Invalid editor command.\n')
            else:
                try:
                    Popen(command, shell=True)
                except OSError:
                    msg = 'Opening editor with command "%s" failed.\n'
                    self._append_plain_text(msg % command)

    def _make_in_prompt(self, number, remote=False):
        """ Given a prompt number, returns an HTML In prompt.
        """
        try:
            body = self.in_prompt % number
        except TypeError:
            # allow in_prompt to leave out number, e.g. '>>> '
            from xml.sax.saxutils import escape
            body = escape(self.in_prompt)
        if remote:
            body = self.other_output_prefix + body
        return '<span class="in-prompt">%s</span>' % body

    def _make_continuation_prompt(self, prompt, remote=False):
        """ Given a plain text version of an In prompt, returns an HTML
            continuation prompt.
        """
        end_chars = '...: '
        space_count = len(prompt.lstrip('\n')) - len(end_chars)
        if remote:
            space_count += len(self.other_output_prefix.rsplit('\n')[-1])
        body = '&nbsp;' * space_count + end_chars
        return '<span class="in-prompt">%s</span>' % body

    def _make_out_prompt(self, number, remote=False):
        """ Given a prompt number, returns an HTML Out prompt.
        """
        try:
            body = self.out_prompt % number
        except TypeError:
            # allow out_prompt to leave out number, e.g. '<<< '
            from xml.sax.saxutils import escape
            body = escape(self.out_prompt)
        if remote:
            body = self.other_output_prefix + body
        return '<span class="out-prompt">%s</span>' % body

    # ------ Payload handlers --------------------------------------------------

    # Payload handlers with a generic interface: each takes the opaque payload
    # dict, unpacks it and calls the underlying functions with the necessary
    # arguments.

    def _handle_payload_edit(self, item):
        self._edit(item['filename'], item['line_number'])

    def _handle_payload_exit(self, item):
        self._keep_kernel_on_exit = item['keepkernel']
        self.exit_requested.emit(self)

    def _handle_payload_next_input(self, item):
        self.input_buffer = item['text']

    def _handle_payload_page(self, item):
        # Since the plain text widget supports only a very small subset of HTML
        # and we have no control over the HTML source, we only page HTML
        # payloads in the rich text widget.
        data = item['data']
        if 'text/html' in data and self.kind == 'rich':
            self._page(data['text/html'], html=True)
        else:
            self._page(data['text/plain'], html=False)

    # ------ Trait change handlers --------------------------------------------

    @observe('style_sheet')
    def _style_sheet_changed(self, changed=None):
        """ Set the style sheets of the underlying widgets.
        """
        self.setStyleSheet(self.style_sheet)
        if self._control is not None:
            self._control.document().setDefaultStyleSheet(self.style_sheet)

        if self._page_control is not None:
            self._page_control.document().setDefaultStyleSheet(self.style_sheet)

    @observe('syntax_style')
    def _syntax_style_changed(self, changed=None):
        """ Set the style for the syntax highlighter.
        """
        if self._highlighter is None:
            # ignore premature calls
            return
        if self.syntax_style:
            self._highlighter.set_style(self.syntax_style)
            self._ansi_processor.set_background_color(self.syntax_style)
        else:
            self._highlighter.set_style_sheet(self.style_sheet)

    # ------ Trait default initializers -----------------------------------------

    @default('banner')
    def _banner_default(self):
        return "Jupyter QtConsole {version}\n".format(version=__version__)


# Clobber IPythonWidget above:

class IPythonWidget(JupyterWidget):
    """Deprecated class; use JupyterWidget."""

    def __init__(self, *a, **kw):
        warn("IPythonWidget is deprecated; use JupyterWidget",
             DeprecationWarning)
        super(IPythonWidget, self).__init__(*a, **kw)
