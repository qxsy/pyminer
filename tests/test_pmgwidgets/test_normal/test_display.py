
if __name__ == '__main__':
    import random
    import sys
    from PyQt5.QtCore import QTimer
    from PyQt5.QtWidgets import QApplication
    from pmgwidgets import DisplayPanel
    app = QApplication(sys.argv)


    def gen_data():
        data_len = 20
        x = {
            'cpu_occupation1': random.randint(0, 100),
            'cpu_occupation2': random.randint(0, 100),
            'cpu_occupation': {'timestamps': [i + 1 for i in range(data_len)],
                               'line1': {'tag': 'CPU利用率1',
                                         'data': [random.randint(0, 100) for i in range(data_len)]},
                               'line2': {'tag': 'CPU利用率2',
                                         'data': [random.randint(0, 100) for i in range(data_len)]},
                               'line3': {'tag': 'CPU利用率3',
                                         'data': [random.randint(0, 100) for i in range(data_len)]}

                               },
            'memory_occupation': {'timestamps': [i + 1 for i in range(data_len)],
                                  'line1': {'tag': '内存利用率1',
                                            'data': [random.randint(0, 100) for i in range(data_len)]},
                                  'line2': {'tag': '内存利用率2',
                                            'data': [random.randint(0, 100) for i in range(data_len)]},
                                  'line3': {'tag': '内存利用率3',
                                            'data': [random.randint(0, 100) for i in range(data_len)]}

                                  },
            'disk_occupation': {'timestamps': [i + 1 for i in range(data_len)],
                                'line1': {'tag': '磁盘利用率1',
                                          'data': [random.randint(0, 100) for i in range(data_len)]},
                                'line2': {'tag': '磁盘利用率2',
                                          'data': [random.randint(0, 100) for i in range(data_len)]},
                                'line3': {'tag': '磁盘利用率3',
                                          'data': [random.randint(0, 100) for i in range(data_len)]}

                                }
        }
        return x


    views = [
        ('gauge', 'cpu_occupation1', 'CPU1占用率', 55, 0.9, (0, 100)),
        # ('gauge', 'cpu_occupation2', 'CPU2占用率', 56, 0.9, (0, 100)),
        ('time_series', 'cpu_occupation', 'CPU占用', {'timestamps': [i + 1 for i in range(10)],
                                                    'line1': {'tag': 'CPU利用率1',
                                                              'data': [random.randint(0, 100) for i in range(10)]},
                                                    'line2': {'tag': 'CPU利用率2',
                                                              'data': [random.randint(0, 100) for i in range(10)]}

                                                    }, (0, 100), '时间', '占用率'),]
    views2 = [
        ('time_series', 'memory_occupation', '内存占用', {'timestamps': [i + 1 for i in range(10)],
                                                      'line1': {'tag': '内存利用率1',
                                                                'data': [random.randint(0, 100) for i in range(10)]},
                                                      'line2': {'tag': '内存利用率2',
                                                                'data': [random.randint(0, 100) for i in range(10)]}

                                                      }, (0, 100), '时间', '占用率'),
        ('time_series', 'disk_occupation', '磁盘IO占用', {'timestamps': [i + 1 for i in range(10)],
                                                      'line1': {'tag': '磁盘利用率1',
                                                                'data': [random.randint(0, 100) for i in range(10)]},
                                                      'line2': {'tag': '磁盘利用率2',
                                                                'data': [random.randint(0, 100) for i in range(10)]}

                                                      }, (0, 100), '时间', '占用率'),]
    views3= [
        ('text', 'cpu_nums', 'CPU数量', str(4)),
        [
            ('table', 'mytable', ['数量', '123456', 'dddddd', 'dddddd'], [['数量', '123456', 'dddddd', 'dddddd']]),
            ('table', 'mytable2', ['数量', '123456', 'dddddd', 'dddddd'], [['数量', '123456', 'dddddd', 'dddddd']])
        ],
    ]
    sp = DisplayPanel(views=views, layout_dir='v')
    sp.set_items(views)
    sp.show()
    sp2 = DisplayPanel(views=views2, layout_dir='v')
    sp2.set_items(views2)
    sp2.show()
    sp3 = DisplayPanel(views=views3, layout_dir='v')
    sp3.set_items(views3)
    sp3.show()
    timer = QTimer()
    timer.start(5000)
    timer.timeout.connect(lambda: sp.set_value(gen_data()))

    sys.exit(app.exec_())
