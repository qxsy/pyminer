import os
import sys
import time
import subprocess
import webbrowser
from typing import Tuple, List

import qdarkstyle
from PyQt5.QtCore import QPoint, QRectF
from PyQt5.QtGui import QMouseEvent, QPainter, QLinearGradient
from PyQt5.QtWidgets import QFrame, QTableWidgetItem, QTableWidget
from qtpy.QtGui import QCloseEvent
from qtpy.QtCore import Signal, Qt, QUrl, QPropertyAnimation
from qtpy.QtWidgets import QApplication, QListWidgetItem, QWizard, QHeaderView
from qtpy.QtWebEngineWidgets import *
from qtpy.QtWidgets import QWidget, QDesktopWidget, QFileDialog, QApplication, QDialog
from pmgwidgets import PMGPanel

from pyminer2.ui.base.option import Ui_Form as Option_Ui_Form
from pyminer2.ui.base.appStore import Ui_Form as appStore_Ui_Form
from pyminer2.ui.base.aboutMe import Ui_Form as About_Ui_Form
from pyminer2.ui.base.project_wizard import Ui_Wizard as Project_Ui_Form
from pyminer2.ui.base.first_form import Ui_Form as first_Ui_Form
from pyminer2.ui.base.pm_marketplace.main import Ui_Form as marketplace_Ui_Form
from pyminer2.ui.base.pm_marketplace.install import Ui_Form as marketplace_install_Ui_Form

from pyminer2.globals import openURL, get_main_window

from pyminer2.features.io.settings import Settings


class OptionForm(QDialog, Option_Ui_Form):
    """
    打开"选项"窗口
    """
    signal_settings_changed = Signal()

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.center()
        self.page_format.setEnabled(False)
        self.page_appearance.setEnabled(False)
        self.page_interpreter.setEnabled(False)

        self.interpreter_table: QTableWidget = self.tableWidget
        self.setup_ui()

        # 通过combobox控件选择窗口风格
        self.comboBox_theme.activated[str].connect(self.slot_theme_changed)

        self.setting = dict()

        self.listWidget.currentRowChanged.connect(self.option_change)
        self.toolButton_workspace.clicked.connect(self.slot_change_workspace)
        self.toolButton_output.clicked.connect(self.slot_change_output)
        self.pushButton_cancel.clicked.connect(self.close)
        self.pushButton_ok.clicked.connect(self.close)
        self.pushButton_help.clicked.connect(self.get_help)

    def setup_ui(self):
        self.comboBox_9.setEnabled(False)
        self.comboBox_8.setEnabled(False)
        self.checkBox_2.setEnabled(False)
        self.checkBox_minitray.setEnabled(False)

    def add_settings_panel(self, text: str, settings_content: List):
        settings_widget = PMGPanel(views=settings_content)
        self.signal_settings_changed.connect(settings_widget.emit_settings_changed_signal)
        self.stackedWidget.addWidget(settings_widget)
        self.listWidget.addItem(QListWidgetItem(text))
        return settings_widget

    def closeEvent(self, a0: 'QCloseEvent') -> None:
        super(OptionForm, self).closeEvent(a0)
        self.refresh_settings()

    def keyPressEvent(self, e):
        """
        按键盘Escape退出当前窗口
        @param e:
        """
        if e.key() == Qt.Key_Escape:
            self.close()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def option_change(self, i):
        self.stackedWidget.setCurrentIndex(i)

    def slot_theme_changed(self, style):
        """
        在主题颜色改变时触发的回调
        :param style:
        :return:
        """
        from pyminer2.features.io.settings import load_theme
        load_theme(style)
        self.refresh_settings()

    def slot_change_workspace(self):
        directory = QFileDialog.getExistingDirectory(self, "选择工作区间位置", )
        if not directory == '':
            self.label_workspace.setText(directory)

    def slot_change_output(self):
        directory = QFileDialog.getExistingDirectory(self, "选择输出文件夹位置", os.path.expanduser('~'))
        self.lineEdit_output.setText(directory)

    def load_settings(self):
        """
        在show()之前调用这个方法
        从而每次重新显示的时候都可以刷新数据。
        :return:
        """
        settings = Settings.get_instance()
        if settings.get('theme') is not None:
            for i in range(self.comboBox_theme.count()):
                if self.comboBox_theme.itemText(i) == settings['theme']:
                    self.comboBox_theme.setCurrentIndex(i)
        self.label_workspace.setText(settings['work_dir'])

    def refresh_settings(self):
        """
        窗口关闭时，调用此方法，刷新主界面设置项。
        :return:
        """
        settings = Settings.get_instance()
        settings['work_dir'] = self.label_workspace.text()
        settings['theme'] = self.comboBox_theme.currentText()
        from pyminer2.globals import get_main_window
        get_main_window().on_settings_changed()
        self.signal_settings_changed.emit()

    def show(self):
        """
        重写此方法，在显示之前重新加载一遍设置。
        :return:
        """
        self.load_settings()
        super(OptionForm, self).show()

    def exec(self):
        self.load_settings()
        super(OptionForm, self).exec()

    def get_help(self):
        webbrowser.open('https://gitee.com/py2cn/pyminer/wikis/%E9%85%8D%E7%BD%AEPyMiner?sort_id=3263840')


class AppstoreForm(QWidget, appStore_Ui_Form):
    def __init__(self):
        super(AppstoreForm, self).__init__()
        self.setupUi(self)
        self.center()

        self.browser = QWebEngineView()
        # 加载外部的web界面
        self.browser.load(QUrl('https://chrome.zzzmh.cn/index#ext'))
        self.horizontalLayout_2.addWidget(self.browser)

        self.toolButton_help.clicked.connect(self.main_help_display)

    def keyPressEvent(self, e):
        """
        按键盘Escape退出当前窗口
        @param e:
        """
        if e.key() == Qt.Key_Escape:
            self.close()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def main_help_display(self):
        """
        打开帮助页面
        """
        try:
            webbrowser.get('chrome').open_new_tab("http://www.pyminer.com")
        except Exception as e:
            webbrowser.open_new_tab("http://www.pyminer.com")


class MarketPlaceInstall(QWidget, marketplace_install_Ui_Form):
    def __init__(self):
        super(MarketPlaceInstall, self).__init__()
        self.setupUi(self)
        self.center()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())


class MarketPlace(QWidget, marketplace_Ui_Form):
    def __init__(self):
        super(MarketPlace, self).__init__()
        self.setupUi(self)

        self.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.center()

        self.pip_list()

        # 绑定事件
        self.btn_install.clicked.connect(self.pip_install_display)

    def keyPressEvent(self, e):
        """
        按键盘Escape退出当前窗口
        @param e:
        """
        if e.key() == Qt.Key_Escape:
            self.close()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def pip_list(self):
        cmd = sys.executable + ' -m pip list'
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        out, err = p.communicate()
        x = 1
        package_str = list()
        for r in out.splitlines():
            if x > 2:
                package_str.append(r)
            x = x + 1

        package_len = len(package_str)
        # 设置表格长度
        self.tableWidget.setRowCount(package_len)

        # 将结果显示在表格中
        for i in range(package_len):
            for j in range(2):
                items_value = package_str[i].split()[j]
                item = QTableWidgetItem(str(items_value, encoding="utf-8"))
                item.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
                self.tableWidget.setItem(i, j, item)

    def pip_install_display(self):
        pm_pack_install = MarketPlaceInstall()
        pm_pack_install.show()


class AboutForm(QWidget, About_Ui_Form):
    def __init__(self):
        super(AboutForm, self).__init__()
        self.setupUi(self)
        self.center()

        self.main_about_display()

    def keyPressEvent(self, e):
        """
        按键盘Escape退出当前窗口
        @param e:
        """
        if e.key() == Qt.Key_Escape:
            self.close()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def main_about_display(self):
        """
        打开关于页面
        """
        import platform
        python_info = 'Python版本: ' + platform.python_version() + ' ' + platform.python_compiler()
        system_info = '系统信息: ' + platform.platform() + ' ' + platform.architecture()[0]
        cpu_info = 'CPU信息: ' + platform.processor()
        self.feedback.setPlainText(python_info + '\n' + system_info + '\n' + cpu_info)


class ProjectWizardForm(QWizard, Project_Ui_Form):
    """
    新建项目引导窗口
    """

    def __init__(self):
        super(ProjectWizardForm, self).__init__()
        self.setupUi(self)
        self.center()
        self.default_setting()

    def keyPressEvent(self, e):
        """
        按键盘Escape退出当前窗口
        @param e:
        """
        if e.key() == Qt.Key_Escape:
            self.close()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def default_setting(self):
        item = self.file_list.item(1)
        item.setSelected(True)


class FirstForm(QDialog, first_Ui_Form):
    """
    快速操作窗口
    """

    def __init__(self, parent=None):
        super(FirstForm, self).__init__(parent)
        self.setupUi(self)
        self.center()
        self.setWindowOpacity(0.95)
        self.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint | Qt.Popup)  # 无边框、弹出式
        self.animation = None
        # self.setStyleSheet("border-radius:10px;border:none;")
        # self.setAttribute(Qt.WA_TranslucentBackground)

        # 绑定事件
        self.btn_manual.clicked.connect(self.open_manual)
        self.btn_website.clicked.connect(self.open_website)
        self.btn_source.clicked.connect(self.open_source)
        self.btn_member.clicked.connect(self.open_member)
        self.btn_donate.clicked.connect(self.open_donate)

        self.btn_open_csv.clicked.connect(self.open_csv)
        self.btn_open_excel.clicked.connect(self.open_excel)
        self.btn_open_matlab.clicked.connect(self.open_matlab)

    def closeEvent(self, event):
        if self.animation is None:
            self.animation = QPropertyAnimation(self, b'windowOpacity', self.parent())
            # self.animation.setPropertyName(b'windowOpacity')
            self.animation.setDuration(200)
            self.animation.setStartValue(self.windowOpacity())
            self.animation.setEndValue(0)
            self.animation.finished.connect(self.close)
            self.animation.start()
            event.ignore()

    def mouseMoveEvent(self, e: QMouseEvent):  # 重写移动事件
        self._endPos = e.pos() - self._startPos
        self.move(self.pos() + self._endPos)
        super(FirstForm, self).mouseMoveEvent(e)

    def mousePressEvent(self, e: QMouseEvent):
        if e.button() == Qt.LeftButton:
            self._isTracking = True
            self._startPos = QPoint(e.x(), e.y())
        super(FirstForm, self).mousePressEvent(e)

    def mouseReleaseEvent(self, e: QMouseEvent):
        super(FirstForm, self).mouseReleaseEvent(e)
        if e.button() == Qt.LeftButton:
            self._isTracking = False
            self._startPos = None
            self._endPos = None

    def keyPressEvent(self, e):
        """
        按键盘Escape退出当前窗口
        @param e:
        """
        super(FirstForm, self).keyPressEvent(e)
        if e.key() == Qt.Key_Escape:
            self.close()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def open_manual(self):
        """
        打开快速入门
        :return:
        """
        openURL("https://gitee.com/py2cn/pyminer/wikis/%E5%85%A5%E9%97%A8%E6%95%99%E7%A8%8B?sort_id=3137860")

    def open_website(self):
        """
        打开快速入门
        :return:
        """
        openURL("http://www.pyminer.com")

    def open_source(self):
        """
        打开快速入门
        :return:
        """
        openURL("https://gitee.com/py2cn/pyminer")

    def open_member(self):
        """
        打开 ‘加入我们’ 页面
        :return:
        """
        openURL("https://gitee.com/py2cn/pyminer/wikis/%E8%81%94%E7%B3%BB%E6%88%91%E4%BB%AC?sort_id=2761039")

    def open_donate(self):
        """
        打开 ‘捐赠’ 页面
        :return:
        """
        openURL("https://gitee.com/py2cn/pyminer/wikis/%E6%8D%90%E8%B5%A0?sort_id=2925146")

    def open_csv(self):
        """
        调用主程序打开csv到工作区间
        :return:
        """
        self.hide()
        get_main_window().process_file('text')
        self.close()

    def open_excel(self):
        """
        调用主程序打开excel到工作区间
        :return:
        """
        self.hide()
        get_main_window().process_file('excel')
        self.close()

    def open_matlab(self):
        """
        调用主程序打开matlab到工作区间
        :return:
        """
        self.hide()
        get_main_window().process_file('matlab')
        self.close()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = FirstForm()
    form.show()
    sys.exit(app.exec())
