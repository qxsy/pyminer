"""
工具汇总。

作者：廖俊易

修改日期：20201215
修改作者：开始说故事
"""

from pyminer2.features.io import sample


class importutils(object):
    def doExcelImport(self):
        self.import_excel_form = sample.ImportExcelForm()
        self.import_excel_form.exec_()

    def doCsvImport(self):
        self.import_csv_form = sample.ImportCsvForm()
        self.import_csv_form.exec_()

    def doTextImport(self):
        self.import_form = sample.ImportTextForm()
        self.import_form.exec_()

    def doSPSSImport(self):
        self.import_spss_form = sample.ImportSpssForm()
        self.import_spss_form.exec_()

    def doSASImport(self):
        self.import_sas_form = sample.ImportSasForm()
        self.import_sas_form.exec_()

    def doMATLABImport(self):
        self.import_matlab_form = sample.ImportMatlabForm()
        self.import_matlab_form.exec_()

    def doImportEngine(self):
        '''
        导入策略列表，后续需要新增导入策略，只需要在前端增加入口，以及导入的策略即可
        :return:
        '''
        ImportEngine = {
            "excel": importutils.doExcelImport,
            "text": importutils.doTextImport,
            "csv": importutils.doCsvImport,
            "spss": importutils.doSPSSImport,
            "sas": importutils.doSASImport,
            "matlab": importutils.doMATLABImport
        }
        return(ImportEngine)
