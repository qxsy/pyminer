import sys
import time
import tornado.platform.asyncio as async_io

from pyminer2.extensions.extensionlib import BaseExtension, BaseInterface
from .ipythonqtconsole import ConsoleWidget

if sys.platform == 'win32':
    async_io.asyncio.set_event_loop_policy(async_io.asyncio.WindowsSelectorEventLoopPolicy())


class Extension(BaseExtension):
    public_interface: 'ConsoleInterface'
    console: 'ConsoleWidget'

    def on_loading(self):
        """在进行加载前，加载翻译文件。"""
        self.extension_lib.Program.add_translation('zh_CN', {'Console': '控制台'})

    def on_load(self):
        """插件加载完成后，绑定控制台控件、数据管理器等变量。"""
        self.console: ConsoleWidget = self.widgets['ConsoleWidget']
        self.console.connect_to_datamanager(self.extension_lib)
        self.extension_lib.Signal.get_settings_changed_signal().connect(self.on_settings_changed)
        self.interface.widget = self.console

    def on_settings_changed(self):
        """
        如果设置项发生改变，重新加载主题文件。
        TODO:倘若ipython发生改变之后，工作路径如何跟着改变？
        """
        settings = self.extension_lib.Program.get_settings()
        self.console.change_ui_theme(settings['theme'])
        self.command = self.interface.run_command("import os;os.chdir(\'%s\')" % settings['work_dir'],
                                                  "工作路径切换为：%s" % settings['work_dir'], hidden=False)


class ConsoleInterface(BaseInterface):
    """
    定义IPython控制台向外开放的函数接口，包括执行一行命令和运行一个文件。
    """
    widget: 'ConsoleWidget'

    def run_command(self, command: str, hint_text='', hidden=True):
        """执行一行命令。

        Args:
            command: 命令。
            hint_text: 在运行代码前显示的提示。
            hidden: 这个参数看起来像是，隐藏执行结果？
        """
        if self.widget is not None:
            self.widget.execute_command(command, hint_text=hint_text, hidden=hidden)

    def run_file(self, file: str):
        """执行一个文件。

        Args:
            file: 文件路径。
        """
        if self.widget is not None:
            self.widget.execute_file(file, True)
