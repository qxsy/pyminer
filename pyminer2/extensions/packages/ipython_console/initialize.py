"""
此文件在IPython启动之时，输入到IPython的变量空间中。
它的作用是预先定义一些函数和魔术方法，定义与工作空间通信的方法。
之后重构可以考虑直接重写IPython.core.interactiveshell.InteractiveShell的各个方法，这样比在这里装配要好一些。
"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import ast
from IPython.core.magic import register_line_magic, register_cell_magic, register_line_cell_magic
from pyminer2.core import *

import typing

if typing.TYPE_CHECKING:
    from IPython.core.interactiveshell import InteractiveShell
    from IPython.core.getipython import get_ipython

__ip: 'InteractiveShell' = get_ipython()
__ip.builtin_vars = [__k for __k in globals().keys()]
__ip.builtin_values = {__k: __v for __k, __v in globals().items() if not __k.startswith('__')}

__ip.var_name_list = []

__ip.neglect_post_run = False


def __filter_vars(__data: dict):
    """
    过滤掉可调用的变量，以防用户使用
    from numpy import *
    这一类操作，造成工作空间不堪重负。
    :param __data:
    :return:
    """
    __keys = []
    for __k in __data.keys():
        if callable(__data[__k]):
            __keys.append(__k)
    for __key in __keys:
        __data.pop(__key)
    return __data


def __refresh_vars():
    """
    刷新工作空间的变量，并且删除在ipython不存在的变量
    :return:
    """
    from pmgwidgets import BaseClient
    import types
    __ip = get_ipython()
    __client = BaseClient()
    __data_message = {}
    __data = {
        __k: __v for __k, __v in globals().items() if
        __k not in __ip.builtin_vars and not __k.startswith('_') and not isinstance(__v, types.ModuleType)
    }
    __data = __ip.filter_vars(__data)
    __name_list = list(__data.keys())
    __deleted_data_name = set(__ip.var_name_list) - set(__name_list)
    __ip.var_name_list = __name_list
    __existing_vars = __client.get_all_public_var_names()

    for __deleted_name in __deleted_data_name:
        if __deleted_name in __existing_vars:
            __client.delete_var(__deleted_name, provider='ipython')


def __update_globals_from_workspace(variables: list = None):
    from pmgwidgets import BaseClient
    __client = BaseClient()
    from pyminer2.core.data.datadesc import DataDesc
    if variables is None:
        globals().update({__k: __v for __k, __v in __client.get_all_vars().items() if not isinstance(__v, DataDesc)})
    else:
        globals().update(
            {__k: __v for __k, __v in __client.get_vars(variables).items() if not isinstance(__v, DataDesc)})


def __delete_var(__var_name: str):
    """
    删除变量。删除变量时不向工作空间发信息（因为这个变量往往来自工作空间）
    :param __var_name:
    :return:
    """
    import types
    __ip = get_ipython()
    if __var_name in globals().keys():
        __unused = globals().pop(__var_name)
        __ip.neglect_post_run = True
        __data = {__k: __v for __k, __v in globals().items()
                  if __k not in __ip.builtin_vars and not __k.startswith('_') and not isinstance(__v, types.ModuleType)}
        __ip.var_name_list = list(__data.keys())


def __post_run_callback():
    """
    IPython代码执行后触发的回调函数
    :return:
    """
    __ip = get_ipython()
    if not __ip.neglect_post_run:
        __ip.refresh_vars()
    else:
        __ip.neglect_post_run = False


def __is_transfer_allowed(__key: str) -> bool:
    import types
    __ip = get_ipython()
    return __key not in __ip.builtin_vars and not __key.startswith('_') and not isinstance(globals().get(__key),
                                                                                           types.ModuleType)


def __pre_run_callback():
    """
    IPython代码执行前触发的回调函数
    :return:
    """
    import types
    __ip = get_ipython()
    if not __ip.neglect_post_run:
        __data = {__k: __v for __k, __v in globals().items()
                  if __k not in __ip.builtin_vars and not __k.startswith('_') and not isinstance(__v, types.ModuleType)}
        __ip.var_name_list = list(__data.keys())

    else:
        __ip.neglect_post_run = True


@register_line_cell_magic
def lcmagic(line, cell=None):
    """
    这是IPython魔术方法的一个例子，可以通过这个来找到例子。
    :param line:
    :param cell:
    :return:
    """
    if cell is None:
        print("Called as line magic")
        return line
    else:
        print("Called as cell magic")
        return line, cell


def __clear_all():
    """
    清除全部变量
    Returns:

    """
    from pmgwidgets import BaseClient
    __ip = get_ipython()
    for __var_name in __ip.var_name_list:
        globals().pop(__var_name)
    __client = BaseClient()
    vars = __client.get_all_public_var_names()
    for __deleted_name in vars:
        __client.delete_var(__deleted_name, provider='ipython')
        import time as _time
        _time.sleep(0.1)

    __ip.neglect_post_run = True


__ip.original_run_cell_func = __ip.run_cell


def __cell_exec_func(raw_cell, store_history=False, silent=False, shell_futures=True):
    """
    相当于重写IPython的执行代码的函数！
    Args:
        raw_cell:
        store_history:
        silent:
        shell_futures:

    Returns:

    """
    import ast, sys
    from pmgwidgets import BaseClient
    from pyminer2.core.data.datadesc import DataDesc
    __ip = get_ipython()

    class PyMinerIPyConsoleNodeTransformer(ast.NodeTransformer):
        def __init__(self):
            super(PyMinerIPyConsoleNodeTransformer, self).__init__()
            self.identifier_list = []
            self.str_list = []

        def visit_Name(self, node: ast.Name):
            """

            Args:
                node:

            Returns:

            """
            self.identifier_list.append(node.id)

        def visit_Str(self, node: ast.Str):
            """

            Args:
                node:

            Returns:

            """
            self.str_list.append(node.s)

        def show_identifiers_might_changed(self, code):
            """

            Args:
                code:

            Returns:

            """
            self.identifier_list = []
            self.str_list = []
            self.visit(ast.parse(code))
            return [s for s in list(set(self.identifier_list + self.str_list)) if s.isidentifier()]

    try:
        import time

        __identifiers = PyMinerIPyConsoleNodeTransformer().show_identifiers_might_changed(raw_cell)
        __identifiers_in_ws = set(BaseClient().get_all_public_var_names())  # identifiers in workspace
        __identifiers_to_update = [__identifier for __identifier in __identifiers if
                                   (__identifier in __identifiers_in_ws)]
        t0 = time.time()
        __ip.update_globals_from_workspace(__identifiers_to_update)
        t1 = time.time()
    except SyntaxError:
        __identifiers = []
    except:
        import traceback
        traceback.print_exc()

    s = get_ipython().original_run_cell_func(raw_cell, store_history=store_history, silent=silent,
                                             shell_futures=shell_futures)
    __var_dic_keys = [__identifier for __identifier in __identifiers if
                      __identifier in globals().keys() and __ip.is_transfer_allowed(__identifier)]

    __var_dic = {}
    for __k in __var_dic_keys:
        __var = globals()[__k]
        if sys.getsizeof(__var) / (1024 ** 2) < 30:
            __var_dic[__k] = __var
        else:
            __var_dic[__k] = DataDesc(__var)
    BaseClient().set_var_dic(__var_dic, 'ipython')
    return s


def __reset(new_session=True):
    __ip = get_ipython()
    __ip.original_reset_func(new_session)

    globals().update(__ip.builtin_values)


__ip.filter_vars = __filter_vars
__ip.refresh_vars = __refresh_vars
__ip.update_globals_from_workspace = __update_globals_from_workspace
__ip.delete_var = __delete_var
__ip.post_run_callback = __post_run_callback
__ip.is_transfer_allowed = __is_transfer_allowed
__ip.pre_run_callback = __pre_run_callback
__ip.clear_all = __clear_all
__ip.run_cell = __cell_exec_func

__ip.original_reset_func = __ip.reset
__ip.reset = __reset
__ip.events.register('post_run_cell', __post_run_callback)
# __ip.events.register('pre_execute', __pre_execute)
# __ip.events.register('pre_run_cell', __pre_run_callback)
