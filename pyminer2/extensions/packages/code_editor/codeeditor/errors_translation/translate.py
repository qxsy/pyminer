import json
import os

d = {}
with open('translations.txt', 'r',encoding='utf-8') as f:
    for l in f.read().split('\n'):
        if l.startswith(('E', 'W', 'C', 'F')):
            try:
                l1 = l.split()
                id = l1[0].strip()
                print(l1, l)
                msg = ' '.join([x.strip() for x in l1[1:]])
                d[id] = msg
            except:
                pass
l.split()
print(d)
path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'flake8_trans.json')
with open(path, 'w') as f:
    json.dump(d, f, indent=4)
