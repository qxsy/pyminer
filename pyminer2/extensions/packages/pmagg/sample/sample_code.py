import matplotlib.pyplot as plt
from matplotlib.pyplot import MultipleLocator
import numpy as np

plt.rcParams['font.sans-serif'] = ['Times new roman']
plt.rcParams['axes.unicode_minus'] = False  # 用来正常显示负号

fig = plt.figure(figsize=(6.4, 4.8),  # 单位英寸
                 dpi=125) # 像素/英寸
xs = np.linspace(0, 10, 50)
ys = np.sin(xs)
plt.plot(xs,
         ys,
         c='red',
         marker='^',
         linestyle=':',
         linewidth=2,
         markerfacecolor='green',
         markeredgecolor='yellow',
         markersize=5,
         alpha=0.7)
plt.xlim(0, 10) # 坐标轴范围
plt.ylim(-1, 1)
plt.tick_params(axis='both', # 可选值 x,y,both，表示操作对象
                which='minor', # 可选值 major minor both 主刻度/次刻度
                labelsize=14,
                top=True, # 显示上边框
                right=True, # 显示右边框
                labelright=True) # 显示有边框刻度值
plt.xlabel('x label', fontsize=14)
plt.ylabel('y label', fontsize=14)
plt.suptitle('sup title')
plt.text(5, 0.6, r'$\mathrm{sin}(t)$', fontsize=20)
x_major_locator = MultipleLocator(1)
y_major_locator = MultipleLocator(0.2)
x_minor_locator = MultipleLocator(0.1)
y_minor_locator = MultipleLocator(0.02)
ax = plt.gca()
# ax 设置坐标轴范围
ax.set_xlim(0, 10)
ax.set_ylim(-1, 1)
ax.set_xlabel('x label')
ax.set_ylabel('y label')
# 设置刻度间隔
ax.xaxis.set_major_locator(x_major_locator)
ax.yaxis.set_major_locator(y_major_locator)
ax.xaxis.set_minor_locator(x_minor_locator)
ax.yaxis.set_minor_locator(y_minor_locator)
# 网格
ax.xaxis.grid(True, which='major')
ax.yaxis.grid(True, which='major')
ax.xaxis.set_tick_params(direction='in', # 坐标轴刻度在里面还是外面，可选值 in out both
                         which='both', # major minor both
                         gridOn=False) # 是否显示网格线
ax.yaxis.set_tick_params(direction='in', which='both', gridOn=False)
# 添加文字
ax.text(6, 0.4, r'note 2', fontsize=20)
plt.show()
