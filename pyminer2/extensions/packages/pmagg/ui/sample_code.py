#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/11/24 12:59
# @Author  : Jiangchenglong
# @Site    : 
# @File    : sample_code.py
# @Software: PyCharm
import os
from PyQt5 import QtWidgets,QtCore,QtGui
from PyQt5.QtWebEngineWidgets import QWebEngineView

class Window(QtWidgets.QDialog):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Sample code')
        layout = QtWidgets.QVBoxLayout()
        self.setLayout(layout)
        path=os.path.join(os.path.dirname(os.path.dirname(__file__)),r'sample\sample_code.html')
        view = QWebEngineView()
        qurl=QtCore.QUrl.fromLocalFile(path)
        view.load(qurl)
        layout.addWidget(view)
        view.show()
        self.exec()