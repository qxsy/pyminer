#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/11/11 14:05
# @Author  : Shark
# @Site    : 
# @File    : arrow_setting.py.py
# @Software: PyCharm
from PyQt5 import QtWidgets
from .linestyles import *

class Window(QtWidgets.QDialog):
    def __init__(self,event, canvas):
        super().__init__()
        self.setWindowTitle('Arrow Setting')
        self.event_arrow = event
        self.canvas = canvas
        self.arrow = event.artist
        self.cancel_button = QtWidgets.QPushButton('取消')
        self.confirm_button = QtWidgets.QPushButton('确认')
        self.layout = QtWidgets.QFormLayout()
        self.generate_items()
        self.arrow_width_spinbox=QtWidgets.QDoubleSpinBox()
        self.arrow_width_spinbox.setRange(0,100)
        self.arrow_style_combox=QtWidgets.QComboBox()
        self.arrow_style_combox.addItems(arrowstyles)
        self.arrow_style_combox.setCurrentText(self.arrow.arrowprops['arrowstyle'])
        self.arrow_width_spinbox.setValue(self.arrow.arrow_patch.get_linewidth())
        self.layout.addRow('线宽', self.arrow_width_spinbox)
        self.layout.addRow('箭头样式', self.arrow_style_combox)
        self.layout.addRow(self.cancel_button, self.confirm_button)
        self.setLayout(self.layout)
        self.confirm_button.clicked.connect(self.confirm_slot)
        self.cancel_button.clicked.connect(self.cancel_slot)
        self.set_equal_width()
        self.exec()

    def generate_items(self):
        pass

    def set_equal_width(self):
        for i in range(self.layout.rowCount()):
            item = self.layout.itemAt(i, self.layout.LabelRole)
            item.widget().setFixedWidth(100)
            item = self.layout.itemAt(i, self.layout.FieldRole)
            item.widget().setFixedWidth(100)

    def confirm_slot(self):
        self.arrow.arrow_patch.set_arrowstyle(self.arrow_style_combox.currentText())
        self.arrow.arrow_patch.set_linewidth(self.arrow_width_spinbox.value())
        self.canvas.draw_idle()

    def cancel_slot(self):
        self.close()