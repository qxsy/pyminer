# -*- coding: utf-8 -*-
# @Time    : 2020/9/4 10:29
# @Author  : 别着急慢慢来
# @FileName: PMAgg.py


import os
import io
import pickle
import numpy as np
from PyQt5 import QtWidgets, QtGui
from PyQt5 import QtCore
from PyQt5.QtGui import QIcon, QCursor, QImage
from matplotlib._pylab_helpers import Gcf
from matplotlib.backend_bases import FigureManagerBase
from matplotlib.backends.backend_qt5 import NavigationToolbar2QT as NavigationToolbar
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.offsetbox import OffsetImage, AnnotationBbox, FancyBboxPatch
from matplotlib.image import BboxImage
import pyqtgraph as pg
import sys
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from .ui import pmagg_ui, axis_edit_manager, default_setting_manager, legend_setting, \
        rectangle_setting, ellipse_setting, colorbar_setting, text_setting, \
        title_setting, arrow_setting, line2d_setting, sample_code, color_table, image_setting
else:
    from pyminer2.extensions.packages.pmagg.ui import pmagg_ui, axis_edit_manager, default_setting_manager, legend_setting, \
        rectangle_setting, ellipse_setting, colorbar_setting, text_setting, \
        title_setting, arrow_setting, line2d_setting, sample_code, color_table, image_setting
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse, Rectangle, FancyArrowPatch
from PyQt5.QtWidgets import QAction, QWidget, QSizePolicy, QMenu, QApplication, QHBoxLayout, QFormLayout
from matplotlib.lines import Line2D
from matplotlib.text import Annotation, Text
from matplotlib.legend import Legend
import matplotlib.font_manager as font_manager
from matplotlib.font_manager import FontProperties
import mpl_toolkits.mplot3d as mpl3d
from matplotlib.colors import to_hex, to_rgb, to_rgba
import matplotlib.font_manager
from matplotlib import rcParams
from pathlib import Path
import re
from functools import partial
import mpl_toolkits.mplot3d.art3d as art3d
from matplotlib.axis import Axis
from matplotlib.axes import Axes
import copy
from matplotlib import rcParams
import webbrowser
from IPython import get_ipython

"""
前提，将该文件所在文件夹注册到path环境变量
1. 永久添加
2. 临时添加
import os
import sys
current_path=os.getcwd()
sys.path.append(current_path)
最好永久添加，这样脚本和控制台都能用


使用时，先申明
import matplotlib
matplotlib.use('module://pmagg')

然后就可以
plt.plot([1,2,3],[4,5,6])
plt.show()


Window类功能需求
1. 拖拽窗口，绘图区始终处于窗口中心，且窗口大小改变时，绘图区能跟着扩展
2. 实现对绘图区坐标轴，图例，标题，文字等各类信息的修改和添加
3. 实现绘图区参数的保存，方便下次调用
4. 实现图片保存
5. 模仿matlab实现更多的功能

Extension 使用时被加载
"""

tab_style_qss = """
QTabWidget::pane{
    border-top:1px solid #C4C4C3;
}
QTabBar::tab{
    background:#e8e8e8;
    border:1px solid #c4c4c3;
    padding:2px;

}
QTabBar::tab:selected, QTabBar::tab:hover {
    background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,
                                stop: 0 #fafafa, stop: 0.4 #f4f4f4,
                                stop: 0.5 #e7e7e7, stop: 1.0 #fafafa);
}

QTabBar::tab:selected {
    border-color: #9B9B9B;
    border-bottom-color: #C2C7CB;
    border-top-color:#6B6B6B;
}
"""


def draw_if_interactive(config_path=None):
    # 设置绘图风格，None和default都用default
    if config_path is None:
        config_path = os.path.join(os.path.dirname(__file__), 'settings.cfg')
    config = QtCore.QSettings(config_path, QtCore.QSettings.IniFormat)
    style = config.value('draw/style')
    plt.style.use('default')  # 防止样式叠加
    try:
        plt.style.use(style)
    except Exception as e:
        config.setValue('draw/style', 'default')
        plt.style.use('default')
    # 预先设置字体
    # 虽然在Window中有set font函数可以设置figure中所有的文字，但文字中含有中文时，控制台会出现很多警告
    # 所以给出默认字体,对消除警告是必须的。
    font_list = ['local', 'mix', 'english']
    man = font_manager.FontManager()
    for item in font_list:
        font = config.value('font/{}_font'.format(item))
        path = config.value('font/{}_font_path'.format(item))
        if font and os.path.exists(path):
            man.addfont(path=path)
            matplotlib.rcParams['font.sans-serif'] = [font]
            matplotlib.rcParams['axes.unicode_minus'] = False  # 解决‘-’bug
            break


class Window(QtWidgets.QMainWindow, pmagg_ui.Ui_MainWindow):
    """
    PMAgg主窗口，PMAgg对matplotlib绘图窗口进行了增强，集成了一些手动添加注释和预览样式的功能
    继承该类实现自定义后端界面，可以自行往menubar和toolbar中添加action，或者添加右键功能菜单
    self.canvas.draw() 每执行该函数，图形重绘
    """

    def __init__(self, tab_page='new', right_menu=True, hide_menubar=False, hide_toolbar=False, config_path=None,
                 icon_path=None):
        """
        当config_path文件中并没有相应的设置项时，tab_page,right_menu等参数项会起作用
        Args:
            tab_page: 可选参数 new,cover。new每次绘图会新建tab页，cover则会在原有的页面绘图，同时隐藏tab pane
            right_menu: 是否开启右键功能，默认开启，关闭则会关闭所有监听事件
            hide_menubar: 隐藏菜单栏
            hide_toolbar: 隐藏工具栏
            config_path: 配置文件路径，没有会创建
        """
        if not QtWidgets.QApplication.instance():
            self.app = QtWidgets.QApplication(sys.argv)  # 需要有这样一句话存在，否则无法super
        else:
            self.app = QtWidgets.QApplication.instance()
        super(Window, self).__init__()
        self.setupUi(self)  # 先执行父类方法，以产生成员变量
        self.retranslateUi(self)
        self.current_path = os.path.dirname(__file__)  # 当前文件路径
        if config_path:  # 配置文件路径
            self.config_path = config_path
        else:
            self.config_path = os.path.join(os.path.dirname(__file__), 'settings.cfg')
        if icon_path:  # 图标文件路径
            self.icon_path = icon_path
        else:
            self.icon_path = os.path.join(self.current_path, 'icons/Icon.ico')
        self.config = QtCore.QSettings(self.config_path, QtCore.QSettings.IniFormat)
        draw_if_interactive(config_path=self.config_path)
        # 设置gui字体
        if self.config.value('font/gui_font'):
            font = QtGui.QFont()
            font.setFamily(self.config.value('font/gui_font'))
            self.app.setFont(font)
        # 设置图标
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(self.icon_path), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.setWindowIcon(icon)
        # 翻译家
        self.trans = QtCore.QTranslator()
        # 所有的标志
        self.flags = {
            'rect': False,
            'oval': False,
            'rotate': False,
            'pan': False,
            'zoom': False,
            'text': False,
            'legend': False,
            'annotation': False,
            'legend_draggable': False,
            'image': False,
            'move': False,
            'add': False,
            'space': True,
            'front': True,
            'back': True,
            'home': True,
            'polygon': False,
        }
        # 工具栏设置

        self.saveAction = QAction(QIcon(os.path.join(self.current_path, 'icons/save.png')), 'save', self)
        self.saveAction.setShortcut('Ctrl+S')
        self.saveAction.triggered.connect(self.save_slot)
        self.toolBar.addAction(self.saveAction)

        self.settingAction = QAction(QIcon(os.path.join(self.current_path, 'icons/setting.png')), 'setting', self)
        self.settingAction.triggered.connect(self.axes_control_slot)
        self.toolBar.addAction(self.settingAction)

        self.spaceAction = QAction(QIcon(os.path.join(self.current_path, 'icons/space.png')), 'space', self)
        self.spaceAction.setShortcut('Ctrl+H')
        self.spaceAction.triggered.connect(lambda: self.event_slot('space', True))
        self.toolBar.addAction(self.spaceAction)

        self.homeAction = QAction(QIcon(os.path.join(self.current_path, 'icons/home.png')), 'home', self)
        self.homeAction.setShortcut('Ctrl+H')
        self.homeAction.triggered.connect(lambda: self.event_slot('home', True))
        self.toolBar.addAction(self.homeAction)

        self.backAction = QAction(QIcon(os.path.join(self.current_path, 'icons/back.png')), 'back', self)
        self.backAction.triggered.connect(lambda: self.event_slot('back', True))
        self.toolBar.addAction(self.backAction)

        self.frontAction = QAction(QIcon(os.path.join(self.current_path, 'icons/front.png')), 'front', self)
        self.frontAction.triggered.connect(lambda: self.event_slot('front', True))
        self.toolBar.addAction(self.frontAction)

        self.zoomAction = QAction(QIcon(os.path.join(self.current_path, 'icons/zoom.png')), 'zoom', self)
        self.zoomAction.triggered.connect(lambda: self.event_slot('zoom', not self.flags['zoom']))
        self.toolBar.addAction(self.zoomAction)

        self.panAction = QAction(QIcon(os.path.join(self.current_path, 'icons/move.png')), 'move axes', self)
        self.panAction.triggered.connect(lambda: self.event_slot('pan', not self.flags['pan']))
        self.toolBar.addAction(self.panAction)

        self.rotateAction = QAction(QIcon(os.path.join(self.current_path, 'icons/rotate.png')), 'rotate', self)
        self.rotateAction.triggered.connect(lambda: self.event_slot('rotate', not self.flags['rotate']))
        self.toolBar.addAction(self.rotateAction)

        self.legendAction = QAction(QIcon(os.path.join(self.current_path, 'icons/legend.png')), '显示/隐藏图例', self)
        self.legendAction.triggered.connect(self.legend_slot)
        self.toolBar.addAction(self.legendAction)

        self.colorbarAction = QAction(QIcon(os.path.join(self.current_path, 'icons/colorbar.png')), '显示/隐藏colorbar',
                                      self)
        self.colorbarAction.triggered.connect(self.show_colorbar_slot)
        self.toolBar.addAction(self.colorbarAction)

        # 文件菜单栏设置
        self.action_save_image.triggered.connect(self.save_slot)
        self.action_save_figure.triggered.connect(self.save_figure_slot)
        self.action_load_figure.triggered.connect(self.load_figure_slot)
        self.action_close.triggered.connect(self.close)
        self.action_save_image.setShortcut('Ctrl+S')
        self.action_save_figure.setShortcut('Ctrl+Shift+S')
        self.action_load_figure.setShortcut('Ctrl+L')
        self.action_close.setShortcut('Ctrl+C')
        # 编辑菜单栏设置
        self.action_copy_figure_to_clipboard.triggered.connect(self.copy_figure_to_clipboard_slot)
        self.action_axis_edit.triggered.connect(self.axis_edit_slot)
        self.action_title_edit.triggered.connect(self.title_edit_slot)
        self.action_default_setting.triggered.connect(self.axes_control_slot)

        # 注释菜单栏设置
        self.action_rectangle.triggered.connect(lambda: self.event_slot('rect', True))
        self.action_oval.triggered.connect(lambda: self.event_slot('oval', True))
        self.action_text.triggered.connect(lambda: self.event_slot('text', True))
        self.action_annotation.triggered.connect(lambda: self.event_slot('annotation', True))
        self.action_arrow.triggered.connect(lambda: self.event_slot('arrow', True))
        self.action_line.triggered.connect(lambda: self.event_slot('line', True))
        self.action_polygon.triggered.connect(lambda: self.event_slot('polygon', True))
        self.action_image.triggered.connect(lambda: self.event_slot('image', True))
        self.action_rectangle.setIcon(QIcon(os.path.join(self.current_path, 'icons/rect.png')))
        self.action_text.setIcon(QIcon(os.path.join(self.current_path, 'icons/text.png')))
        self.action_oval.setIcon(QIcon(os.path.join(self.current_path, 'icons/oval.png')))
        self.action_annotation.setIcon(QIcon(os.path.join(self.current_path, 'icons/annotation.png')))
        self.action_arrow.setIcon(QIcon(os.path.join(self.current_path, 'icons/arrow.png')))
        self.action_line.setIcon(QIcon(os.path.join(self.current_path, 'icons/line.png')))
        self.action_polygon.setIcon(QIcon(os.path.join(self.current_path, 'icons/polygon.png')))
        self.action_image.setIcon(QIcon(os.path.join(self.current_path, 'icons/image.png')))
        self.action_rectangle.setShortcut('Alt+R')
        self.action_text.setShortcut('Alt+T')
        self.action_oval.setShortcut('Alt+O')
        self.action_annotation.setShortcut('Alt+X')
        self.action_arrow.setShortcut('Alt+A')
        self.action_line.setShortcut('Alt+L')
        self.action_polygon.setShortcut('Alt+P')
        self.action_image.setShortcut('Alt+I')

        # 视图菜单栏设置
        self.action_main_view.triggered.connect(partial(self.change_view_slot, azim=0, elev=0))
        self.action_left_view.triggered.connect(partial(self.change_view_slot, azim=90, elev=0))
        self.action_right_view.triggered.connect(partial(self.change_view_slot, azim=-90, elev=0))
        self.action_bottom_view.triggered.connect(partial(self.change_view_slot, azim=0, elev=90))
        self.action_top_view.triggered.connect(partial(self.change_view_slot, azim=0, elev=-90))
        self.action_back_view.triggered.connect(partial(self.change_view_slot, azim=180, elev=0))
        self.action45_45_view.triggered.connect(partial(self.change_view_slot, azim=45, elev=45))
        self.action45_m45_view.triggered.connect(partial(self.change_view_slot, azim=45, elev=-45))
        self.action_m45_45_view.triggered.connect(partial(self.change_view_slot, azim=-45, elev=45))
        self.action_m45_m45_view.triggered.connect(partial(self.change_view_slot, azim=-45, elev=-45))

        # 帮助菜单栏设置
        self.action_help.triggered.connect(self.help_slot)
        self.action_matplotlib.triggered.connect(self.open_mpl_website_slot)
        self.action_code.triggered.connect(self.show_sample_code_slot)
        self.action_color_table.triggered.connect(self.show_color_table_slot)
        self.action_help.setShortcut('F1')
        self.action_matplotlib.setShortcut('F3')
        self.action_code.setShortcut('F2')

        # set page widget
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setTabsClosable(True)
        self.tabWidget.setObjectName("tabWidget")
        self.gridLayout.addWidget(self.tabWidget, 2, 0, 1, 1)
        self.tabWidget.tabCloseRequested.connect(self.close_tab_slot)
        self.tabWidget.currentChanged.connect(self.switch_tab_slot)
        self.tabWidget.setStyleSheet(tab_style_qss)

        # 提示label 放在statusbar中，显示矩形或点的坐标等
        self.label = QtWidgets.QLabel()
        self.toolBar.addWidget(self.label)

        self.add_rect_flag = False
        self.add_oval_flag = False
        self.add_text_flag = False
        self.rotate_flag = False
        self.pan_flag = False
        self.zoom_flag = False
        self.show_legend_flag = False
        self.add_annotation_flag = False
        self.move_event_flag = False
        # 当前的artist对象
        self.current_artist = None
        # 这里面装着可以被home去除的组件
        self.can_remove_elements = []
        self.current_subplot = None
        self.current_artist_move_flag = False
        self.legend_draggable_flag = False
        # 这个button的出现是因为在添加文字时，明明鼠标移动时，没有按下按钮，但是mpl识别出按下了按钮，所以另外做一个标志
        self.button = 0
        self.canvases = []
        self.canvas = None
        self.toolbar = None
        # page 页
        self.tab_page_title_index = 1
        # pyqtgraph
        self.win = pg.GraphicsLayoutWidget()

    def init_figure(self):
        self.set_subplots()
        # 所有的按钮标志
        self.make_flag_invalid()
        self.read_all_settings()
        self.set_pickers()
        self.set_all_fonts()
        self.set_mpl_events()
        self.set_gui_language()
        self.set_tabs()

    def set_tabs(self):
        flag = self.config.value('draw/tab') == 'new'
        if flag:
            self.tabWidget.tabBar().show()
        else:
            self.tabWidget.tabBar().hide()

    def set_gui_language(self):
        if self.config.value('language/current_language') == 'en':
            self._trigger_english()
        if self.config.value('language/current_language') == 'zh_CN':
            self._trigger_zh_cn()

    def _trigger_english(self):
        """切换英语，并且写入配置文件"""
        self.current_language = 'en'
        en_qm = os.path.join(self.current_path, 'langs/en_pmagg_ui.qm')
        self.trans.load(en_qm)
        _app = QApplication.instance()  # 获取app实例
        _app.installTranslator(self.trans)  # 重新翻译主界面
        self.retranslateUi(self)
        self.config.setValue('language/current_language', 'en')

    def _trigger_zh_cn(self):
        self.current_language = 'zh_CN'
        zh_qm = os.path.join(self.current_path, 'langs/zh_CN_pmagg_ui.qm')
        self.trans.load(zh_qm)
        _app = QApplication.instance()
        _app.installTranslator(self.trans)
        self.retranslateUi(self)
        self.config.setValue('language/current_language', 'zh_CN')

    def set_subplots(self):
        """给子图combobox和菜单栏中的子图menu设置项"""
        # 获取子图对象
        self.axes = self.canvas.figure.get_axes()
        if not self.axes:
            QtWidgets.QMessageBox.warning(
                self.canvas.parent(), "Error", "There are no axes to edit.")
            return
        elif len(self.axes) == 1:
            self.current_subplot, = self.axes
            titles = ['子图1']
        else:
            titles = ['子图' + str(i + 1) for i in range(len(self.axes))]
            self.current_subplot = self.axes[0]
        # 将三维图的初始视角保存下来，便于旋转之后可以复原
        self.init_views = [(index, item.azim, item.elev) for index, item in enumerate(self.axes) if
                           hasattr(item, 'azim')]
        self.menu.clear()
        actions = []
        for index, title in enumerate(titles):
            action = QtWidgets.QAction(self)
            action.setObjectName('action_subplot_' + str(index))
            action.setText(title)
            action.setCheckable(True)
            action.triggered.connect(partial(self.menu_subplots_slot, index))
            actions.append(action)
        self.menu.addActions(actions)
        self.menu.actions()[0].setChecked(True)

    def set_mpl_events(self):
        # 鼠标拖拽，实现三维图形旋转功能
        self.canvas.mpl_connect('button_press_event', self.on_rotate)
        self.canvas.mpl_connect('motion_notify_event', self.on_rotate)

        # 为图例绑定监听事件
        self.canvas.mpl_connect('axes_enter_event', self.drag_legend)
        self.canvas.mpl_connect('axes_leave_event', self.drag_legend)
        # 为右键功能集成
        self.canvas.mpl_connect('pick_event', self.right_button_menu)
        # 移动事件
        self.canvas.mpl_connect('motion_notify_event', self.move_event)
        self.canvas.mpl_connect('button_release_event', self.move_event)
        self.canvas.mpl_connect('pick_event', self.move_event)
        # 添加事件
        self.canvas.mpl_connect('button_press_event', self.add_event)
        self.canvas.mpl_connect('motion_notify_event', self.add_event)
        self.canvas.mpl_connect('button_release_event', self.add_event)

    def get_canvas(self, figure):
        if self.canvas:
            old_canvas = self.canvas
        self.canvas = FigureCanvas(figure)  # 这里的canvas就是曲线图
        self.init_figure()  # 先初始化图形，设置项等
        if self.canvas.figure._suptitle:
            tab_page_title = self.canvas.figure._suptitle.get_text()
        else:
            tab_page_title = 'figure ' + str(self.tab_page_title_index)
        if self.config.value('draw/tab') == 'new':
            tab_widget_page = QtWidgets.QWidget()
            grid_layout = QtWidgets.QGridLayout(tab_widget_page)
            self.canvases.append(self.canvas)
            self.tabWidget.addTab(tab_widget_page, tab_page_title)
            grid_layout.addWidget(self.canvas)
            grid_layout.removeWidget(self.canvas)
            grid_layout.addWidget(self.canvas)
            self.tab_page_title_index += 1
            self.tabWidget.setCurrentWidget(tab_widget_page)
        if self.config.value('draw/tab') == 'cover':
            if self.tabWidget.count() == 0:  # 还未产生tab页
                tab_widget_page = QtWidgets.QWidget()
                grid_layout = QtWidgets.QGridLayout(tab_widget_page)
                self.canvases.append(self.canvas)
                self.tabWidget.addTab(tab_widget_page, tab_page_title)
                grid_layout.addWidget(self.canvas)
                self.tab_page_title_index += 1
            else:
                self.canvases[self.tabWidget.currentIndex()] = self.canvas  # 更改当前页的canvas
                self.tabWidget.currentWidget().layout().removeWidget(old_canvas)
                self.tabWidget.currentWidget().layout().addWidget(self.canvas)

    def close_tab_slot(self, index):
        self.tabWidget.removeTab(index)
        self.canvases.pop(index)

    def switch_tab_slot(self, index):
        """切换tab页之后，无需重新读取setting，因为所有的setting都是通用的"""
        try:
            if self.tabWidget.count() != 0:
                self.canvas = self.canvases[index]
                self.set_subplots()
        except Exception as e:
            pass

    def select_font(self, text: Text):
        content = text.get_text()
        zh_model = re.compile(u'[\u4e00-\u9fa5]')
        en_model = re.compile(u'[a-z]')
        zh = zh_model.search(content)
        en = en_model.search(content)
        if zh and en and self.config.value('font/mix_font') and os.path.exists(self.config.value('font/mix_font_path')):
            text.set_fontproperties(Path(self.config.value('font/mix_font_path')))
            return text
        if zh and not en and self.config.value('font/local_font') and os.path.exists(
                self.config.value('font/local_font_path')):
            text.set_fontproperties(Path(self.config.value('font/local_font_path')))
            return text
        if not zh and self.config.value('font/english_font') and os.path.exists(
                self.config.value('font/english_font_path')):
            text.set_fontproperties(Path(self.config.value('font/english_font_path')))
            return text
        return text

    def read_all_settings(self):
        """对一些默认设置进行预处理"""
        # 选择绘图方式
        if not self.config.value('draw/tab'):
            self.config.setValue('draw/tab', 'new')
        if not self.config.value('draw/width'):
            self.config.setValue('draw/width', str(self.canvas.figure.get_figwidth()))
        if not self.config.value('draw/height'):
            self.config.setValue('draw/height', str(self.canvas.figure.get_figheight()))
        if not self.config.value('draw/dpi'):
            self.config.setValue('draw/dpi', str(self.canvas.figure.get_dpi()))
        if not self.config.value('draw/style'):
            self.config.setValue('draw/style', 'default')
        # 选择语言
        if not self.config.value('language/current_language'):
            self.config.setValue('language/current_language', 'en')
        # 上次打开的路径
        if not os.path.exists(str(self.config.value('path/last_path'))):
            self.config.setValue('path/last_path', os.path.expanduser('~'))
        if self.config.value('close/clear_history') is None:
            self.config.setValue('close/clear_history', "True")

    def set_pickers(self, flag=True):
        """将所有的对象都设置成可pick的对象，使能被事件响应，这里有一个特别大的陷阱，如果存在可picker的对象，那么figure将无法pickle！！"""
        for ax in self.axes:
            for line in ax.lines:
                line.set_picker(flag)
            for patch in ax.patches:
                patch.set_picker(flag)
            for text in ax.texts:
                text.set_picker(flag)
            legend = ax.get_legend()
            ax.set_picker(flag)
            if legend:
                legend.set_picker(flag)

    def set_all_fonts(self):
        if self.canvas.figure._suptitle:
            self.select_font(self.canvas.figure._suptitle)
        for ax in self.axes:
            for text in ax.texts:
                self.select_font(text)
            for label in ax.get_xticklabels():  # make the xtick labels pickable
                self.select_font(label)
            for label in ax.get_yticklabels():  # make the ytick labels pickable
                self.select_font(label)
            self.select_font(ax.title)
            self.select_font(ax.xaxis.label)
            self.select_font(ax.yaxis.label)
            if hasattr(ax, 'zaxis'):
                self.select_font(ax.zaxis.label)
            legend = ax.get_legend()
            if legend:
                for text in legend.texts:
                    self.select_font(text)
            if hasattr(ax, 'get_zticklabels()'):
                for label in ax.get_zticklabels():
                    self.select_font(label)

    def set_figure(self):
        self.canvas.figure.set_figwidth(float(self.config.value('draw/width')))
        self.canvas.figure.set_figheight(float(self.config.value('draw/height')))
        self.canvas.figure.set_dpi(float(self.config.value('draw/dpi')))

    def make_flag_invalid(self):
        self.add_rect_flag = False
        self.add_oval_flag = False
        self.add_text_flag = False
        self.rotate_flag = False
        self.home_flag = False
        self.pan_flag = False
        self.zoom_flag = False
        self.show_legend_flag = False
        self.add_annotation_flag = False
        self.move_event_flag = False
        self.legend_draggable_flag = False

    def menu_annotation_selected(self, menu_action: QAction):
        """将当前的menu选中，其余关闭"""
        for menu in self.menu_annotation.actions():
            if menu != menu_action:
                menu.setChecked(False)

    def copy_figure_to_clipboard_slot(self):
        with io.BytesIO() as buffer:
            self.canvas.figure.savefig(buffer)
            QApplication.clipboard().setImage(QImage.fromData(buffer.getvalue()))

    def home_slot(self):
        """matplotlib lines里面放曲线，patches可以放图形，artists也可以放东西，设为空则可以删除对应的对象"""
        # 运行QT5Agg原来的home，实现平移的复位
        self.toolbar.home()
        # 将三维图视角还原
        for item in self.init_views:
            self.axes[item[0]].view_init(azim=item[1], elev=item[2])
        # 将can_remove_elements中的对应的元素全部删除
        for i in self.can_remove_elements:
            i.remove()
            for item in self.axes:
                if i in item.texts:
                    item.texts.remove(i)
                if i in item.artists:
                    item.artists.remove(i)
                if i in item.patches:
                    item.patches.remove(i)
                if i in item.lines:
                    item.lines.remove(i)
                if i in item.collections:
                    item.lines.remove(i)
        self.can_remove_elements.clear()
        self.canvas.draw()

    def event_slot(self, flag: str, status) -> None:
        """
        完成所有事件按钮的切换工作，每次重新产生qt5agg原来的toolbar
        Args:
            flag: 标志
            status: 标志要切换过去的状态

        Returns:

        """
        if not self.canvas.toolbar:
            self.toolbar = NavigationToolbar(self.canvas, self)
            self.toolbar.hide()  # 隐藏QT原来的工具栏
        else:
            self.toolbar = self.canvas.toolbar
        self.toolbar.mode = None  # 禁用移动和缩放
        self.current_artist = None
        self.canvas.setCursor(QtCore.Qt.ArrowCursor)
        for item in self.flags.keys():
            if item == flag:
                self.flags[item] = status
            else:
                self.flags[item] = False
        for menu in self.menu_annotation.actions():
            menu.setChecked(False)
        if status:
            if flag == 'zoom':
                self.toolbar.zoom()
            if flag == 'pan':
                self.toolbar.pan()
            if flag == 'rotate':
                self.canvas.setCursor(QtCore.Qt.SizeAllCursor)
            if flag == 'space':
                self.toolbar.configure_subplots()
            if flag == 'front':
                self.toolbar._nav_stack.forward()
                self.toolbar._update_view()
            if flag == 'back':
                self.toolbar._nav_stack.back()
                self.toolbar._update_view()
            if flag == 'home':
                self.home_slot()
            if flag == 'rect':
                self.action_rectangle.setChecked(True)
            if flag == 'oval':
                self.action_oval.setChecked(True)
            if flag == 'text':
                self.action_text.setChecked(True)
            if flag == 'annotation':
                self.action_annotation.setChecked(True)
            if flag == 'arrow':
                self.action_arrow.setChecked(True)
            if flag == 'line':
                self.action_line.setChecked(True)
            if flag == 'polygon':
                self.action_polygon.setChecked(True)
            if flag == 'legend':
                self.legend_slot()
            if flag == 'image':
                self.action_image.setChecked(True)

    def save_slot(self):
        """
        在按默认参数保存图片后，将图片大小重新调整到窗口实际大小。
        """
        width = self.canvas.figure.get_figwidth()
        height = self.canvas.figure.get_figheight()
        dpi = self.canvas.figure.get_dpi()
        self.set_figure()
        self.toolbar.save_figure()
        self.canvas.figure.set_figwidth(width)
        self.canvas.figure.set_figheight(height)
        self.canvas.figure.set_dpi(dpi)
        self.canvas.draw_idle()

    def save_figure_slot(self):
        """保存figure的pickle对象到文件，一定要先关闭picker"""
        file_name, ok = QtWidgets.QFileDialog.getSaveFileName(self, 'Save figure object',
                                                              self.config.value('path/last_path'),
                                                              filter="Pickle object (*.pickle)")
        if file_name != '':
            if file_name.split('.')[-1] != 'pickle':  # 添加后缀
                file_name = file_name + '.pickle'
            with open(file_name, 'wb') as f:
                self.set_pickers(flag=False)
                pickle.dump(self.canvas.figure, f)
            self.set_pickers(True)
        dir_name = os.path.dirname(file_name)
        self.config.setValue('path/last_path', dir_name)

    def load_figure_slot(self):
        name = QtWidgets.QFileDialog.getOpenFileName(self, 'Open figure object', self.last_path,
                                                     filter='Pickle object (*.pickle)')
        if os.path.exists(name[0]):
            with open(name[0], 'rb') as f:
                self.get_canvas(pickle.load(f))
        dir_name = os.path.dirname(name[0])
        self.last_path = dir_name
        self.set_config('path', 'last_path', self.last_path)

    def on_rotate(self, event):
        if not self.flags['rotate']:
            return
        # 如果鼠标移动过程有按下，视为拖拽，判断当前子图是否有azim属性来判断当前是否3D
        if event.button == 1 and hasattr(event.inaxes, 'azim'):
            self.canvas.setCursor(QtCore.Qt.SizeAllCursor)
            if event.name == 'button_press_event':
                pseudo_bbox = event.inaxes.transLimits.inverted().transform([(0, 0), (1, 1)])
                self._pseudo_w, self._pseudo_h = pseudo_bbox[1] - pseudo_bbox[0]
                self.mouse_x, self.mouse_y = event.xdata, event.ydata
                self.axes3d_azim = event.inaxes.azim
                self.axes3d_elev = event.inaxes.elev
            if event.name == 'motion_notify_event' and self.axes3d_azim:
                delta_azim = -180 * (event.xdata - self.mouse_x) / self._pseudo_w
                delta_elev = -180 * (event.ydata - self.mouse_y) / self._pseudo_h
                azim = delta_azim + self.axes3d_azim
                elev = delta_elev + self.axes3d_elev
                event.inaxes.view_init(azim=azim, elev=elev)
                self.canvas.draw_idle()

    def move_event(self, event):
        if not self.flags['move']:
            return
        if event.name == 'pick_event' and event.mouseevent.button == 1 and self.current_artist is None:
            """点击到对象，先将该对象移除，并保存背景"""
            if type(event.artist) in [Rectangle, Ellipse, Text, Annotation, AnnotationBbox]:
                self.current_artist_move_flag = True
                self.current_artist = event.artist
                if type(event.artist) in [Rectangle, Ellipse]:
                    self.current_artist_dx = self.current_artist.get_width()
                    self.current_artist_dy = self.current_artist.get_height()
                    self.current_artist_alpha = self.current_artist.get_alpha()
                    self.current_artist.set_alpha(0)
                if type(event.artist) == Text:
                    self.current_artist_alpha = self.current_artist.get_alpha()
                    self.current_artist.set_alpha(0)
                    self.bbox = self.current_artist.get_bbox_patch()
                    if self.bbox is not None:
                        self.bbox_alpha = self.bbox.get_alpha()
                        self.bbox.set_alpha(0)
                if type(event.artist) == AnnotationBbox:
                    self.current_artist_alpha = self.current_artist.get_alpha()
                    for item in self.current_artist.get_children():
                        if type(item) == FancyBboxPatch:
                            item.set_alpha(0)
                        if type(item) == OffsetImage:
                            item.image.set_alpha(0)

                    self.current_artist.set_alpha(0)
                if type(event.artist) == Annotation:
                    self.current_artist_text = self.current_artist.get_text()
                    self.current_artist.set_text('')
                    self.current_artist_alpha = self.current_artist.arrow_patch.get_alpha()
                    self.current_artist.arrow_patch.set_alpha(0)
                    xdata, ydata = event.mouseevent.xdata, event.mouseevent.ydata
                    distance_xy = np.std(np.array((xdata, ydata)) - np.array(self.current_artist.xy))
                    distance_xyann = np.std(np.array((xdata, ydata)) - np.array(self.current_artist.xyann))
                    if distance_xy < distance_xyann:
                        self.annotaion_flag = 'xyann'
                    else:
                        self.annotaion_flag = 'xy'
                self.canvas.draw()
                self.background = self.canvas.copy_from_bbox(event.mouseevent.inaxes.bbox)
        if isinstance(event, matplotlib.backend_bases.MouseEvent):
            if event.inaxes and event.button == 1 and event.name == 'motion_notify_event' and not hasattr(event.inaxes,
                                                                                                          'azim'):
                if type(self.current_artist) in [Rectangle, Ellipse, Text, AnnotationBbox]:
                    self.current_artist.set_alpha(self.current_artist_alpha)
                    if type(self.current_artist) == Rectangle:
                        center = (event.xdata + self.current_artist_dx / 2, event.ydata + self.current_artist_dy / 2)
                        self.current_artist.set_xy((event.xdata, event.ydata))
                        self.label.setText('Rect Center:(%0.2f,%0.2f) Width=%0.2f Height=%0.2f Area=%0.2f' % (
                            center[0], center[1], np.abs(self.current_artist_dx), np.abs(self.current_artist_dy),
                            np.abs(self.current_artist_dx * self.current_artist_dy)))
                    if type(self.current_artist) == Ellipse:
                        center = (event.xdata, event.ydata)
                        self.current_artist.set_center(center)
                        self.label.setText('Oval:Center:(%0.2f,%0.2f) Width=%0.2f Height=%0.2f Area=%0.2f' % (
                            center[0], center[1], np.abs(self.current_artist_dx), np.abs(self.current_artist_dy),
                            np.abs(np.pi * self.current_artist_dx * self.current_artist_dy / 4)))
                    if type(self.current_artist) == Text:
                        if self.bbox:  # 恢复边框透明度
                            self.bbox.set_alpha(self.bbox_alpha)
                        self.current_artist.set_position((event.xdata, event.ydata))
                        self.label.setText('Text Position:(%0.2f,%0.2f)' % (event.xdata, event.ydata))
                    if type(self.current_artist) == AnnotationBbox:
                        for item in self.current_artist.get_children():
                            if type(item) == OffsetImage:
                                item.image.set_alpha(1)
                            else:
                                item.set_alpha(1)
                        self.current_artist.xybox = (event.xdata, event.ydata)
                    self.canvas.restore_region(self.background)
                    event.inaxes.draw_artist(self.current_artist)
                    self.canvas.blit(event.inaxes.bbox)

                if type(self.current_artist) == Annotation:
                    self.current_artist.arrow_patch.set_alpha(self.current_artist_alpha)
                    if self.annotaion_flag == 'xy':
                        self.current_artist.xyann = (event.xdata, event.ydata)
                    else:
                        self.current_artist.xy = (event.xdata, event.ydata)
                    if self.current_artist_text != '':
                        self.current_artist.set_text("x:%.2f\ny:%.2f" % self.current_artist.xy)
                    self.canvas.restore_region(self.background)
                    event.inaxes.draw_artist(self.current_artist)
                    self.canvas.blit(event.inaxes.bbox)
            if event.name == 'button_release_event' and type(self.current_artist) in [Rectangle, Ellipse, Text,
                                                                                      Annotation]:
                """鼠标放开，将current_artist重新添加到canvas，并且令动画效果为False"""
                self.current_artist.set_animated(False)
                self.current_artist_dx = None
                self.current_artist_dy = None
                self.current_artist_alpha = None
                self.label.setText('')
                self.current_artist = None
                self.current_artist_move_flag = False
                self.image_alpha = None
                self.bbox = None
                self.bbox = None

    def add_event(self, event):
        if self.action_rectangle.isChecked():
            if event.inaxes and event.button == 1 and not hasattr(event.inaxes, 'azim'):
                if event.name == 'button_press_event':
                    self.event_init = event
                    self.current_artist = Rectangle((self.event_init.xdata, self.event_init.ydata), 0, 0, fill=False,
                                                    edgecolor='red', linewidth=1, picker=True)
                    event.inaxes.add_artist(self.current_artist)  # 这里不能使用event.inaxes.patches.append方法
                    self.current_artist.set_animated(True)
                    self.can_remove_elements.append(self.current_artist)
                    self.background = self.canvas.copy_from_bbox(event.inaxes.bbox)
                    event.inaxes.draw_artist(self.current_artist)
                    self.canvas.blit(event.inaxes.bbox)
                if event.name == 'motion_notify_event' and isinstance(self.current_artist, Rectangle):
                    width = event.xdata - self.event_init.xdata
                    height = event.ydata - self.event_init.ydata
                    center = (self.event_init.xdata + width / 2, self.event_init.ydata + height / 2)
                    self.current_artist.set_width(width)
                    self.current_artist.set_height(height)
                    self.label.setText('Rect Center:(%0.2f,%0.2f) Width=%0.2f Height=%0.2f Area=%0.2f' % (
                        center[0], center[1], np.abs(width), np.abs(height), np.abs(width * height)))
                    self.canvas.restore_region(self.background)
                    event.inaxes.draw_artist(self.current_artist)
                    self.canvas.blit(event.inaxes.bbox)
            if event.name == 'button_release_event' and isinstance(self.current_artist, Rectangle):
                """鼠标放开，令动画效果为False"""
                self.current_artist.set_animated(False)
                self.label.setText('')
                self.current_artist = None
                self.action_rectangle.setChecked(False)
                self.flags['move'] = True

        if self.action_oval.isChecked():
            if event.inaxes and event.button == 1 and not hasattr(event.inaxes, 'azim'):
                if event.name == 'button_press_event':
                    self.event_init = event
                    self.current_artist = Ellipse(xy=(self.event_init.xdata, self.event_init.ydata),
                                                  width=abs(event.xdata - self.event_init.xdata) * 2,
                                                  height=abs(event.ydata - self.event_init.ydata) * 2, angle=0,
                                                  fill=False,
                                                  edgecolor='red', linewidth=1, picker=True)
                    event.inaxes.add_artist(self.current_artist)  # 这里不能使用event.inaxes.patches.append方法
                    self.current_artist.set_animated(True)
                    self.can_remove_elements.append(self.current_artist)
                    self.background = self.canvas.copy_from_bbox(event.inaxes.bbox)
                    event.inaxes.draw_artist(self.current_artist)
                    self.canvas.blit(event.inaxes.bbox)
                if event.name == 'motion_notify_event' and isinstance(self.current_artist, Ellipse):
                    width = 2 * (event.xdata - self.event_init.xdata)
                    height = 2 * (event.ydata - self.event_init.ydata)
                    center = self.current_artist.get_center()
                    self.current_artist.set_width(width)
                    self.current_artist.set_height(height)
                    self.label.setText('Oval:Center:(%0.2f,%0.2f) Width=%0.2f Height=%0.2f Area=%0.2f' % (
                        center[0], center[1], np.abs(width), np.abs(height), np.abs(np.pi * width * height / 4)))
                    self.canvas.restore_region(self.background)
                    event.inaxes.draw_artist(self.current_artist)
                    self.canvas.blit(event.inaxes.bbox)
            if event.name == 'button_release_event' and isinstance(self.current_artist, Ellipse):
                """鼠标放开，令动画效果为False"""
                self.current_artist.set_animated(False)
                self.label.setText('')
                self.current_artist = None
                self.action_oval.setChecked(False)
                self.flags['move'] = True

        if self.action_text.isChecked():
            if event.inaxes and event.button == 1 and not hasattr(event.inaxes, 'azim'):
                if event.name == 'button_press_event':
                    text, ok = QtWidgets.QInputDialog.getText(self.canvas.parent(), '输入文字', '添加注释')
                    if ok:
                        self.event_init = event
                        self.current_artist = event.inaxes.text(event.xdata, event.ydata, text)
                        self.current_artist.set_picker(True)
                        self.can_remove_elements.append(self.current_artist)
                        self.select_font(self.current_artist)
                        event.inaxes.texts.append(self.current_artist)
                        self.button = 0
                        self.canvas.draw()
                        self.label.setText('')
                        self.current_artist = None
                        self.action_text.setChecked(False)
                        self.flags['move'] = True

        if self.action_image.isChecked():
            if event.inaxes and event.button == 1 and not hasattr(event.inaxes, 'azim'):
                if event.name == 'button_press_event':
                    image_path, _ = QtWidgets.QFileDialog.getOpenFileName(self.centralwidget,
                                                                          "openImage", "",
                                                                          "*.jpg;;*.png;;All Files(*)")
                    if image_path:
                        image = plt.imread(image_path)
                        bbox = self.current_subplot.get_window_extent().transformed(
                            self.canvas.figure.dpi_scale_trans.inverted())
                        ax_width, ax_height = bbox.width, bbox.height
                        ax_width *= self.canvas.figure.dpi
                        ax_height *= self.canvas.figure.dpi
                        image_width = image.shape[0]
                        zoom = 0.2 * ax_width / image_width
                        imagebox = OffsetImage(image, zoom=zoom)
                        self.current_artist = AnnotationBbox(imagebox, (event.xdata, event.ydata))
                        self.current_artist.set_picker(True)
                        event.inaxes.add_artist(self.current_artist)
                        self.can_remove_elements.append(self.current_artist)
                        self.canvas.draw()
                        self.current_artist = None
                        self.action_image.setChecked(False)
                        self.flags['move'] = True

        if self.action_arrow.isChecked():
            if event.inaxes and event.button == 1 and not hasattr(event.inaxes, 'azim'):
                if event.name == 'button_press_event':
                    self.event_init = event
                    self.current_artist = event.inaxes.annotate(
                        "",
                        xy=(self.event_init.xdata, self.event_init.ydata),
                        xytext=(self.event_init.xdata, self.event_init.ydata),
                        arrowprops=dict(arrowstyle='->'),
                        picker=True)
                    event.inaxes.texts.append(self.current_artist)  # 这里不能使用event.inaxes.patches.append方法
                    self.current_artist.set_animated(True)
                    self.can_remove_elements.append(self.current_artist)
                    self.background = self.canvas.copy_from_bbox(event.inaxes.bbox)
                    event.inaxes.draw_artist(self.current_artist)
                    self.canvas.blit(event.inaxes.bbox)
                if event.name == 'motion_notify_event' and type(self.current_artist) == Annotation:
                    width = 2 * (event.xdata - self.event_init.xdata)
                    height = 2 * (event.ydata - self.event_init.ydata)
                    self.current_artist.xy = (event.xdata, event.ydata)
                    self.label.setText('Arrow:Position:(%0.2f,%0.2f) Length=%0.2f' % (
                        event.xdata, event.ydata, np.sqrt(width ** 2 + height ** 2)))
                    self.canvas.restore_region(self.background)
                    event.inaxes.draw_artist(self.current_artist)
                    self.canvas.blit(event.inaxes.bbox)
            if event.name == 'button_release_event' and type(self.current_artist) == Annotation:
                """鼠标放开，令动画效果为False"""
                self.current_artist.set_animated(False)
                self.label.setText('')
                self.current_artist = None
                self.action_arrow.setChecked(False)
                self.flags['move'] = True

        if self.action_annotation.isChecked():
            if event.inaxes and event.button == 1 and not hasattr(event.inaxes, 'azim'):
                if event.name == 'button_press_event':
                    x_range = event.inaxes.get_xlim()
                    y_range = event.inaxes.get_ylim()
                    offset = ((x_range[1] - x_range[0]) * 0.05, (y_range[1] - y_range[0]) * 0.05)
                    self.current_artist = event.inaxes.annotate(
                        "x:%.2f\ny:%.2f" % (event.xdata, event.ydata),
                        xy=(event.xdata, event.ydata),
                        xytext=(event.xdata + offset[0], event.ydata + offset[1]),
                        bbox=dict(
                            boxstyle='round,pad=0.5',
                            fc='white',
                            alpha=0.5,
                            linewidth=1,
                            edgecolor='blue'),
                        arrowprops=dict(arrowstyle='->'),
                        picker=True)
                    self.set_all_fonts()
                    event.inaxes.texts.append(self.current_artist)
                    self.can_remove_elements.append(self.current_artist)
                    self.canvas.draw()
            if event.name == 'button_release_event' and type(self.current_artist) == Annotation:
                """鼠标放开，令动画效果为False"""
                self.label.setText('')
                self.current_artist = None
                self.action_annotation.setChecked(False)
                self.flags['move'] = True

        if self.action_line.isChecked():
            if event.inaxes and event.button == 1 and not hasattr(event.inaxes, 'azim'):
                if event.name == 'button_press_event':
                    self.event_init = event
                    self.current_artist = event.inaxes.annotate(
                        "",
                        xy=(self.event_init.xdata, self.event_init.ydata),
                        xytext=(self.event_init.xdata, self.event_init.ydata),
                        arrowprops=dict(arrowstyle='-'),
                        picker=True)
                    event.inaxes.texts.append(self.current_artist)  # 这里不能使用event.inaxes.patches.append方法
                    self.current_artist.set_animated(True)
                    self.can_remove_elements.append(self.current_artist)
                    self.background = self.canvas.copy_from_bbox(event.inaxes.bbox)
                    event.inaxes.draw_artist(self.current_artist)
                    self.canvas.blit(event.inaxes.bbox)
                if event.name == 'motion_notify_event' and type(self.current_artist) == Annotation:
                    width = 2 * (event.xdata - self.event_init.xdata)
                    height = 2 * (event.ydata - self.event_init.ydata)
                    self.current_artist.xy = (event.xdata, event.ydata)
                    self.label.setText('Arrow:Position:(%0.2f,%0.2f) Length=%0.2f' % (
                        event.xdata, event.ydata, np.sqrt(width ** 2 + height ** 2)))
                    self.canvas.restore_region(self.background)
                    event.inaxes.draw_artist(self.current_artist)
                    self.canvas.blit(event.inaxes.bbox)
            if event.name == 'button_release_event' and type(self.current_artist) == Annotation:
                """鼠标放开，令动画效果为False"""
                self.current_artist.set_animated(False)
                self.label.setText('')
                self.current_artist = None
                self.action_line.setChecked(False)
                self.flags['move'] = True

    def legend_slot(self):
        if self.flags['legend']:
            if self.current_subplot.lines:  # 如果存在曲线才允许画图例
                legend = self.current_subplot.get_legend()
                if legend:
                    legend.remove()
                self.show_legend_flag = False
                self.canvas.draw()
        else:
            legend_titles = []
            for index in range(len(self.current_subplot.lines)):
                label = self.current_subplot.lines[index]._label
                if label.startswith('_line'):
                    legend_titles.append('curve ' + str(index + 1))
                else:
                    legend_titles.append(label)
            if self.current_subplot.lines:  # 如果存在曲线才允许画图例
                leg = self.current_subplot.legend(self.current_subplot.lines, legend_titles)
                leg.set_draggable(True)  # 设置legend可拖拽
                for legline in leg.get_lines():
                    legline.set_pickradius(10)
                    legline.set_picker(True)  # 给每个legend设置可点击
                self.canvas.draw()
                self.show_legend_flag = True

    def show_colorbar_slot(self):
        colorbar_setting.Window(self.canvas)

    def change_view_slot(self, azim, elev):
        if not hasattr(self.current_subplot, 'azim'):
            return
        self.current_subplot.view_init(azim=azim, elev=elev)
        self.canvas.draw()

    def menu_subplots_slot(self, index):
        """将menu子图和当前子图对象关联起来"""
        for action in self.menu.actions():
            action.setChecked(False)
        self.menu.actions()[index].setChecked(True)
        self.current_subplot = self.axes[index]  # 将当前选择付给子图对象

    def axes_control_slot(self):
        if not self.current_subplot:
            QtWidgets.QMessageBox.warning(
                self.canvas.parent(), "错误", "没有可选的子图！")
            return
        default_setting_manager.Ui_Form_Manager(self)
        draw_if_interactive(config_path=self.config_path)

    def axis_edit_slot(self):
        axis_edit_manager.Ui_Dialog_Manager(ax=None, canvas=self.canvas, config=self.config, path=self.current_path)

    def title_edit_slot(self):
        title_setting.Window(self.canvas)

    def delete_artist(self, event: matplotlib.backend_bases.PickEvent):
        """删除对象，既要调用Artist.remove，又要从axes.xxs中删除该对象才能删除"""
        event.artist.remove()
        if event.artist in self.can_remove_elements:
            self.can_remove_elements.remove(event.artist)
        if event.artist in event.mouseevent.inaxes.artists:
            event.mouseevent.inaxes.artists.remove(event.artist)
        if event.artist in event.mouseevent.inaxes.lines:
            event.mouseevent.inaxes.lines.remove(event.artist)
        if event.artist in event.mouseevent.inaxes.patches:
            event.mouseevent.inaxes.patches.remove(event.artist)
        if event.artist in event.mouseevent.inaxes.collections:
            event.mouseevent.inaxes.collections.remove(event.artist)
        self.canvas.draw()

    def generate_trend_line(self, event):
        assert isinstance(event.artist, Line2D)
        deg, ok = QtWidgets.QInputDialog.getInt(self, '趋势线对话框', '拟合次数')
        xs, ys = event.artist.get_data()
        xs = xs.reshape(-1)
        ys = ys.reshape(-1)
        parameter = np.polyfit(xs, ys, deg)
        # 对参数进行拼接
        label = '$'
        for i in range(deg + 1):
            bb = round(parameter[i], 6)  # bb是i次项系数
            if bb >= 0:
                if i == 0:
                    bb = str(bb)
                else:
                    bb = ' +' + str(bb)
            else:
                bb = ' ' + str(bb)
            if deg == i:
                label = label + bb
            else:
                if deg - i != 1:
                    label = label + bb + 'x^' + str(deg - i)
                else:
                    label = label + bb + 'x'
        label += '$'
        f = np.poly1d(parameter)
        event.mouseevent.inaxes.plot(xs, f(xs), 'r--', label=label)
        self.canvas.draw_idle()

    def drag_legend(self, event):
        """鼠标进入axes，默认legend可以拖拽，离开axes，使之不可拖拽，因为legend可拖拽会导致figure无法pickle"""
        ax = event.inaxes
        legend = ax.get_legend()
        self.legend_draggable_flag = not self.legend_draggable_flag
        if legend:
            legend.set_draggable(self.legend_draggable_flag, use_blit=True)

    def right_button_menu(self, event):
        """"""
        if event.mouseevent.button == 3:
            # if isinstance(event.artist,matplotlib.patches.Wedge):
            #     print(event.artist.get_edgecolor())
            #     print(event.artist.get_facecolor())
            #     print(event.artist.theta1)
            #     print(event.artist.theta2)
            #     print(event.artist.r)
            #     print(event.artist.center)
            #     print(event.artist)
            # if isinstance(event.artist, Axes):
            #     self.contextMenu = QMenu()
            #     axes = event.artist
            #     action_axis = self.contextMenu.addAction('修改坐标轴样式')
            #     action_axis.triggered.connect(lambda: axis_setting.Window(event, self.canvas))
            #     self.contextMenu.popup(QCursor.pos())
            #     self.contextMenu.show()
            if isinstance(event.artist, Legend):
                self.contextMenu = QMenu()
                action_visible = self.contextMenu.addAction('显示/隐藏')
                action_style = self.contextMenu.addAction('修改图例样式')
                action_visible.triggered.connect(self.legend_slot)
                action_style.triggered.connect(lambda: legend_setting.Window(event, self.canvas))
                self.contextMenu.popup(QCursor.pos())
                self.contextMenu.show()
            if isinstance(event.artist, Rectangle):
                self.contextMenu = QMenu()
                action_delete = self.contextMenu.addAction('删除矩形')
                action_style = self.contextMenu.addAction('修改矩形样式')
                action_delete.triggered.connect(lambda: self.delete_artist(event))
                action_style.triggered.connect(lambda: rectangle_setting.Window(event, self.canvas))
                self.contextMenu.popup(QCursor.pos())
                self.contextMenu.show()
            if isinstance(event.artist, Ellipse):
                self.contextMenu = QMenu()
                action_delete = self.contextMenu.addAction('删除椭圆')
                action_style = self.contextMenu.addAction('修改椭圆样式')
                action_delete.triggered.connect(lambda: self.delete_artist(event))
                action_style.triggered.connect(lambda: ellipse_setting.Window(event, self.canvas))
                self.contextMenu.popup(QCursor.pos())
                self.contextMenu.show()
            if isinstance(event.artist, Line2D):
                self.contextMenu = QMenu()
                action_delete = self.contextMenu.addAction('删除')
                action_style = self.contextMenu.addAction('修改曲线样式')
                action_trend_line = self.contextMenu.addAction('添加趋势线')
                action_delete.triggered.connect(lambda: self.delete_artist(event))
                action_style.triggered.connect(lambda: line2d_setting.Window(event, self.canvas))
                action_trend_line.triggered.connect(lambda: self.generate_trend_line(event))
                self.contextMenu.popup(QCursor.pos())
                self.contextMenu.show()
            if isinstance(event.artist, Annotation):
                self.contextMenu = QMenu()
                action_delete = self.contextMenu.addAction('删除箭头')
                action_style = self.contextMenu.addAction('修改箭头样式')
                action_delete.triggered.connect(lambda: self.delete_artist(event))
                action_style.triggered.connect(lambda: arrow_setting.Window(event, self.canvas))
                self.contextMenu.popup(QCursor.pos())
                self.contextMenu.show()
            if type(event.artist) == Text:
                self.contextMenu = QMenu()
                action_delete = self.contextMenu.addAction('删除文字')
                action_style = self.contextMenu.addAction('修改文字样式')
                action_delete.triggered.connect(lambda: self.delete_artist(event))
                action_style.triggered.connect(lambda: text_setting.Window(event, self.canvas))
                self.contextMenu.popup(QCursor.pos())
                self.contextMenu.show()
            if type(event.artist) == AnnotationBbox:
                self.contextMenu = QMenu()
                action_delete = self.contextMenu.addAction('删除图片')
                action_style = self.contextMenu.addAction('修改图片样式')
                action_delete.triggered.connect(lambda: self.delete_artist(event))
                action_style.triggered.connect(lambda: image_setting.Window(event, self.canvas))
                self.contextMenu.popup(QCursor.pos())
                self.contextMenu.show()

    def help_slot(self):
        webbrowser.open("https://gitee.com/py2cn/pyminer/tree/master/pyminer2/extensions/packages/pmagg")

    def open_mpl_website_slot(self):
        webbrowser.open("https://matplotlib.org/")

    def show_sample_code_slot(self):
        sample_code.Window()

    def show_color_table_slot(self):
        color_table.Window()

    def closeEvent(self, a0: QtGui.QCloseEvent) -> None:
        """我也不知道为什么需要重写closeEvent而不是重写close方法"""
        # 清除历史绘图
        if self.config.value('close/clear_history') == 'True':
            self.canvases = []
            self.canvas = None
            self.tab_page_title_index = 1
            self.tabWidget.clear()

    def show(self):
        # 使pmagg每次绘图之后，窗口在最前
        self.activateWindow()
        self.setWindowState(self.windowState() & ~QtCore.Qt.WindowMinimized | QtCore.Qt.WindowActive)
        super(Window, self).show()
        ipython = get_ipython()
        if ipython is not None:
            ipython.magic("gui qt5")
        else:
            QtWidgets.QApplication.instance().exec()


class FigureManagerPyMiner(FigureManagerBase):
    agg = Window()  # 必须是静态变量，防止被回收

    def show(self):
        FigureManagerPyMiner.agg.get_canvas(self.canvas.figure)
        FigureManagerPyMiner.agg.show()


"""下面这堆可以不用管"""


def show(*, block=None):
    """
    For image backends - is not required.
    For GUI backends - show() is usually the last line of a pyplot script and
    tells the backend that it is time to draw.  In interactive mode, this
    should do nothing.
    """
    for manager in Gcf.get_all_fig_managers():
        # do something to display the GUI
        # t = threading.Thread(target=manager.show())
        # t.daemon = True
        # t.start()
        manager.show()
        Gcf.destroy(manager.num)


def new_figure_manager(num, *args, FigureClass=Figure, **kwargs):
    """Create a new figure manager instance."""
    # If a main-level app must be created, this (and
    # new_figure_manager_given_figure) is the usual place to do it -- see
    # backend_wx, backend_wxagg and backend_tkagg for examples.  Not all GUIs
    # require explicit instantiation of a main-level app (e.g., backend_gtk3)
    # for pylab.
    thisFig = FigureClass(*args, **kwargs)
    return new_figure_manager_given_figure(num, thisFig)


def new_figure_manager_given_figure(num, figure):
    """Create a new figure manager instance for the given figure."""
    # canvas = FigureCanvasTemplate(figure)
    # manager = FigureManagerTemplate(canvas, num)
    # return manager
    from matplotlib.backends.backend_agg import FigureCanvasAgg
    canvas = FigureCanvasAgg(figure)
    manager = FigureManagerPyMiner(canvas, num)
    return manager
