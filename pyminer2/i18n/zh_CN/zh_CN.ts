<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="app2.py" line="307"/>
        <source>Files</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="app2.py" line="320"/>
        <source>Welcome to PyMiner</source>
        <translation>欢迎使用PyMiner</translation>
    </message>
    <message>
        <location filename="app2.py" line="326"/>
        <source>Plugs</source>
        <translation>插件</translation>
    </message>
</context>
<context>
    <name>PMToolBarHome</name>
    <message>
        <location filename="app2.py" line="102"/>
        <source>New Script</source>
        <translation>新建脚本</translation>
    </message>
    <message>
        <location filename="app2.py" line="106"/>
        <source>New</source>
        <translation>新建</translation>
    </message>
    <message>
        <location filename="app2.py" line="111"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="app2.py" line="116"/>
        <source>Get Data</source>
        <translation>获取数据</translation>
    </message>
    <message>
        <location filename="app2.py" line="120"/>
        <source>Import Database</source>
        <translation>从数据库导入</translation>
    </message>
    <message>
        <location filename="app2.py" line="124"/>
        <source>Load Var</source>
        <translation>加载变量</translation>
    </message>
    <message>
        <location filename="app2.py" line="124"/>
        <source>Save Var</source>
        <translation>保存变量</translation>
    </message>
    <message>
        <location filename="app2.py" line="124"/>
        <source>Clear Var</source>
        <translation>清除变量</translation>
    </message>
    <message>
        <location filename="app2.py" line="131"/>
        <source>Extensions</source>
        <translation>扩展中心</translation>
    </message>
    <message>
        <location filename="app2.py" line="135"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="app2.py" line="137"/>
        <source>Community</source>
        <translation>社区</translation>
    </message>
    <message>
        <location filename="app2.py" line="140"/>
        <source>Layout</source>
        <translation>布局</translation>
    </message>
    <message>
        <location filename="app2.py" line="142"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="app2.py" line="190"/>
        <source>Support</source>
        <translation>官方网站</translation>
    </message>
    <message>
        <location filename="app2.py" line="194"/>
        <source>Reference</source>
        <translation>帮助文档</translation>
    </message>
    <message>
        <location filename="app2.py" line="199"/>
        <source>Check for updates</source>
        <translation>检查更新</translation>
    </message>
    <message>
        <location filename="app2.py" line="206"/>
        <source>Give Feedback</source>
        <translation>反馈</translation>
    </message>
    <message>
        <location filename="app2.py" line="210"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="app2.py" line="219"/>
        <source>New Project</source>
        <translation>新建项目</translation>
    </message>
    <message>
        <location filename="app2.py" line="222"/>
        <source>Text Data</source>
        <translation>文本数据</translation>
    </message>
    <message>
        <location filename="app2.py" line="225"/>
        <source>CSV Data</source>
        <translation>CSV数据</translation>
    </message>
    <message>
        <location filename="app2.py" line="228"/>
        <source>Excel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="app2.py" line="231"/>
        <source>SAS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="app2.py" line="234"/>
        <source>SPSS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="app2.py" line="237"/>
        <source>MATLAB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="app2.py" line="240"/>
        <source>STATA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="app2.py" line="243"/>
        <source>Encode converter</source>
        <translation>编码转换</translation>
    </message>
    <message>
        <location filename="app2.py" line="246"/>
        <source>MySQL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="app2.py" line="249"/>
        <source>Oracle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="app2.py" line="252"/>
        <source>PostgreSQL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
