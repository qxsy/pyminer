from typing import Any


class DataDesc():
    def __init__(self, data: Any):
        self.type = get_data_type(data)
        self.size = get_size(data)
        repr_value = get_repr(data)
        self.repr_value = repr_value if len(repr_value) < 1000 else repr_value[:1000]


def get_size(data: Any):
    """
    获取数据的尺寸。
    有‘shape’这一属性的，size就等于data.shape
    有‘__len__’这个属性的，返回len(data)
    否则返回1.
    Args:
        data:

    Returns:

    """
    size = 1
    if hasattr(data, 'shape'):
        size = data.shape
    elif hasattr(data, '__len__'):
        try:
            size = len(data)
        except:
            pass
    return size


def get_data_type(data: Any):
    data_type = str(type(data)).replace('class', '').replace(' ', '').replace('\'', '').replace('<',
                                                                                                '').replace('>', '')
    if data_type == 'pandas.core.frame.DataFrame':
        data_type = 'DataFrame'
    elif data_type == 'pandas.core.series.Series':
        data_type = 'Series'
    return data_type


def get_repr(data: Any):
    if isinstance(data, (list, tuple, str)):
        if len(data) > 1000:
            return repr(data[:1000])
    return repr(data)
